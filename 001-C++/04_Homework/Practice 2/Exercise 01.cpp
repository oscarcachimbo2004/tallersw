/**
 * @file Exercise 01.cpp
 * @author Oscar Lopez (oscar.lopez10@unmsm.edu.pe)
 * @brief Calcular el pago semanal de un trabajador. Los datos a ingresar son: Total de horas trabajadas 
 		  y el pago por hora.
		  Si el total de horas trabajadas es mayor a 40 la diferencia se considera como horas extras y se paga 
		  un 50% mas que una hora normal.
          Si el sueldo bruto es mayor a s/. 500.00, se descuenta un 10% en caso contrario el descuento es 0.

 * @version 1.6
 * @date 5.01.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include<iostream>
using namespace std;

/*******************************************************************************************************************************************
 *  									
 *******************************************************************************************************************************************/
float inputTotalHours = 0.0;
float inputHourly = 0.0;

float correspondingPayment = 0.0;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void CheckTexas();
float CalculatePay(float totalHours, float hourly);



/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	CheckTexas();
}
//=====================================================================================================

void CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"Calculation in a course\r\n";
	cout<<"\tEnter the total hours worked: ";
	cin>>inputTotalHours;
	cout<<"\tEnter hourly pay: ";
	cin>>inputHourly;
}
//=====================================================================================================

void Calculate(){
	correspondingPayment = CalculatePay(inputTotalHours, inputHourly); //su pago pero sin impuestos 
	
}
//=====================================================================================================

void CheckTexas(){
	
	cout<<"============Show results============\r\n";
	if(correspondingPayment>500){
		float fullpay = 0.0; // Se descuenta
		fullpay = correspondingPayment * (90.0/100.0);
		
		cout<<"\tWe deduct taxes...\r\n ";
		cout<<"\tPayment is: S/."<<fullpay<<"\r\n";
	}else{
		cout<<"\r\n \tWe don't discount anything...\r\n \tEnjoy your day";
		cout<<"\n\r \tPayment is: S/."<<correspondingPayment<<"\r\n";
	}
	
	
}

//=====================================================================================================

float CalculatePay(float totalHours, float hourly){
	float partialPayment = 0.0; //Parcial hasta que se verifique si se descuenta o no
	float extraHours = 0.0;
	float extraPayment = 0.0;
	
	if(totalHours > 40){
		extraHours = totalHours - 40;
		extraPayment = (50.0*extraHours*hourly)/100.0;
		partialPayment = totalHours * hourly + extraPayment;
	}else{
		partialPayment = totalHours * hourly; 
	}
	
	return partialPayment;
}


