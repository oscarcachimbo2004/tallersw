/**
 * @file Exercise 05.cpp
 * @author Oscar Lopez (oscar.lopez10@unmsm.edu.pe)
 * @brief Dados tres datos enteros positivos, que representen las longitudes de un posible triangulo, determine si los datos corresponden a un triangulo. 
 		  En caso afirmativo, escriba si el triangulo es equil�tero, is�sceles o escaleno. Calcule adem�s su �rea
 * @version 1.9
 * @date 27.11.2021
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include<iostream>
#include<math.h>
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float inputSide1 = 0.0;
float inputSide2 = 0.0;
float inputSide3 = 0.0;

float area = 0.0;
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void CheckTriangle();
float CalculateArea(float side1,float side2, float side3);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	CheckTriangle();
}
//=====================================================================================================

void CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"Calculation in a course\r\nTriangle sides...\r\n Side 1 , Side 2 and Side3\r\n";
	cout<<"\tType the first side: ";
	cin>>inputSide1;
	cout<<"\tType the second side: ";
	cin>>inputSide2;
	cout<<"\tType the third side: ";
	cin>>inputSide3;
}
//=====================================================================================================

void Calculate(){
	area = CalculateArea(inputSide1,inputSide2,inputSide3);
}
//=====================================================================================================

void CheckTriangle(){
	cout<<"============Check Triangle============\r\n";
	
	if (((inputSide1 + inputSide2)>inputSide3) && ((inputSide1 + inputSide3) > inputSide2) && ((inputSide3 + inputSide2) > inputSide1)){
		
		if(inputSide1 == inputSide2 && inputSide1 == inputSide3){
			cout<<"It's an equilateral triangle."<<"\r\n";
		}
		else if(inputSide1 == inputSide2 || inputSide2 == inputSide3 || inputSide1 ==inputSide3){
			cout<<"It's an isosceles triangle."<<"\r\n";
		}
		else{
			cout<<"It's a scalene triangle."<<"\r\n";
		}
		
		cout<<"The area of the triangle is: "<<area<<" square units\r\n";
	}
	
	else{
		cout<<"The digitized triangle doesn't exist :(\r\nTherefore, there is no area";
	}
	
}
//=====================================================================================================

float CalculateArea(float side1,float side2, float side3){
	float area = 0.0;
	float semiperimeter = 0.0;
	
	semiperimeter = (side1+side2+side3)/2.0;
	area = sqrt(semiperimeter * (semiperimeter-side1) * (semiperimeter-side2) * (semiperimeter-side3));
	
	return area;
}
