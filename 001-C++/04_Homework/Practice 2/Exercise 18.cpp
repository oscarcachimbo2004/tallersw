/**
 * @file Exercise 18.cpp
 * @author Oscar Lopez (oscar.lopez10@unmsm.edu.pe)
 * @brief En una tienda de descuento se efect�a una promoci�n en la cual se hace un descuento sobre el valor de la compra total seg�n 
 		  el color de la bolita que el cliente saque al pagar en caja. Si la bolita es de color blanco no se le har� descuento alguno, 
		  si es verde se le har� un 10% de descuento, si es amarilla un 25%, si es azul un 50% y si es roja un 100%. Determinar la 
		  cantidad final que el cliente deber� pagar por su compra. se sabe que solo hay bolitas de los colores mencionados.
 * @version 1.0
 * @date 30.04.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <stdlib.h>
#include <time.h>
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int globalColorNumber = 0;
float globalDiscount = 0.0;
float globalPurchase = 0.0;

float globalTotalToPay = 0.0;
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
bool CollectData();
bool Calculate();
bool ShowResults();

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	if(Run() == false){
		cout<<"Error running the program\r\n";
	}
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

bool Run(){
	while(true){
		if(CollectData() == false){
			cout<<"\r\nError requesting data, please try again\r\n";
			continue;
		}
		if(Calculate() == false){
			cout<<"\r\nError calculating, please try again\r\n";
			continue;
		}
		if(ShowResults() == false){
			cout<<"\r\nError displaying data, try again\r\n";
			continue;
		}
		break;
	}
	
	return true;
}
//=====================================================================================================

bool CollectData(){
	cout<<"\tWelcome to Oscar's menu,"<<"\r\n";
	cout<<"\tI will help you to calculate the total to pay,"<<"\r\n";
	cout<<"\tThanks to the color you will choose"<<"\r\n";
	cout<<"\tEnter the total of your purchase: ";
	cin>>globalPurchase;
	
	srand(time(NULL)); //genera un numero aleatorio
	
	globalColorNumber = 1 + rand() % (5);
	
	return true;
}
//=====================================================================================================

bool Calculate(){
	
	if(globalColorNumber == 1){
		globalDiscount = globalPurchase * 0;
		cout<<"\tThe chosen color is white";
	}
	else if(globalColorNumber == 2){
		globalDiscount = globalPurchase * 0.1;
		cout<<"\tThe chosen color is green";
	}
	else if(globalColorNumber == 3){
		globalDiscount = globalPurchase * 0.25;
		cout<<"\tThe chosen color is yelow";
	}
	else if(globalColorNumber == 4){
		globalDiscount = globalPurchase * 0.5;
		cout<<"\tThe chosen color is blue";
	}
	else if(globalColorNumber == 5){
		globalDiscount = globalPurchase * 1;
		cout<<"\tThe chosen color is red";
	}
	
	globalTotalToPay = globalPurchase - globalDiscount;
	
	return true;
}
//=====================================================================================================

bool ShowResults(){
	cout<<"\r\n============Show result============\r\n";
	
	if(globalColorNumber != 5){
		cout<<"\tThe profit received by the worker is: S/."<<globalTotalToPay<<"\r\n";
	}
	else{
		cout<<"You pay nothing!!";
	}
	return true;
}
