/**
 * @file Exercise 09.cpp
 * @author Oscar Lopez (oscar.lopez10@unmsm.edu.pe)
 * @brief Calcular la comisi�n sobre las ventas totales de un empleado, sabiendo que el empleado no recibe comisi�n si su venta es hasta 
  S/.150,si la venta es superior a S/.150 y menor o igual a S/.400 el empleado recibe una comisi�n del 10% de las ventas y si las ventas 
	son mayores a 400, entonces la comisi�n es de S/.50 m�s el 9% de las ventas.
 * @version 1.8
 * @date 23.02.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES 
 *******************************************************************************************************************************************/
float globalInputSales = 0.0;

float globalcommission = 0.0;
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
bool CollectData();
bool Calculate();
bool ShowResults();

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	if(Run() == false){
		cout<<"Error running the program\r\n";
	}
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

bool Run(){
	while(true){
		if(CollectData() == false){
			cout<<"\r\nError requesting data, please try again\r\n";
			continue;
		}
		if(Calculate() == false){
			cout<<"\r\nError calculating, please try again\r\n";
			continue;
		}
		if(ShowResults() == false){
			cout<<"\r\nError displaying data, try again\r\n";
			continue;
		}
		break;
	}
	
	return true;
}
//=====================================================================================================

bool CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"Calculation in a course\r\n";
	cout<<"\tEnter the total sales: ";
	cin>>globalInputSales;
	
	//Verificamos el dato
	if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n');
		globalInputSales = 0.0;
		cout<<"The amount entered is not valid.\r\n";
		return false;
	}
	
	//Verificamos si no es negativo por si hay algun gracioso
	if(globalInputSales < 0){
		cout<<"The amount entered is not valid, it cannot be negative\r\n";
		return false;
	}
	
	return true;
}
//=====================================================================================================

bool Calculate(){
	
	if((globalInputSales > 150) && (globalInputSales <= 400)){
		globalcommission = 0.1 * globalInputSales;
	}
	if(globalInputSales > 400){
		globalcommission = 50.0 + globalInputSales * 0.09;
	} 
	return true;
}
//=====================================================================================================

bool ShowResults(){
	cout<<"============Show result============\r\n";
	if(globalInputSales <= 150){
		cout<<"Didn't reach the minimum sales....\r\nDoesn't receive commission\r\n";
	}
	else{
		cout<<"The commission that will be awarded is: S/."<<globalcommission<<"\r\n";
	}
	
	return true;
}
