/**
 * @file Exercise 12.cpp
 * @author Oscar Lopez (oscar.lopez10@unmsm.edu.pe)
 * @brief Calcular la utilidad que un trabajador recibe en el reparto anual de utilidades si este se le asigna como un porcentaje de su
 			salario mensual que depende de su antig�edad en la empresa de acuerdo con la sig. tabla:
 			Tiempo									Utilidad
	Menos de 1 a�o								5 % del salario
	1 a�o o mas y menos de 2 a�os				7% del salario
	2 a�os o mas y menos de 5 a�os				10% del salario
	5 a�os o mas y menos de 10 a�os				15% del salario
	10 a�os o mas								20% del salario
	
 * @version 1.8
 * @date 27.11.2021
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float globalInputSalary = 0.0;
float globalInputSeniorityInMonths = 0.0;

float globalUtility = 0.0;
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
bool CollectData();
bool Calculate();
bool ShowResults();

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	if(Run() == false){
		cout<<"Error running the program\r\n";
	}
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

bool Run(){
	while(true){
		if(CollectData() == false){
			cout<<"\r\nError requesting data, please try again\r\n";
			continue;
		}
		if(Calculate() == false){
			cout<<"\r\nError calculating, please try again\r\n";
			continue;
		}
		if(ShowResults() == false){
			cout<<"\r\nError displaying data, try again\r\n";
			continue;
		}
		break;
	}
	
	return true;
}
//=====================================================================================================

bool CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"Calculation in a course\r\n";
	cout<<"\tEnter the monthly salary: ";
	cin>>globalInputSalary;
	
	//Verificamos el dato
	if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n');
		globalInputSalary = 0.0;
		cout<<"salary isn't valid.\r\n";
		return false;
	}
	//Verificamos si es valido para que no haya explotacion
	if(globalInputSalary < 930){
		cout<<"The minimum salary must be 930 soles....\r\nFriend...\r\nDon't let yourself explode!\r\n";
		return false;
	}
	cout<<"\tEnter the years of seniority: ";
	cin>>globalInputSeniorityInMonths;
	
	//Verificamos el dato
	if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n');
		globalInputSeniorityInMonths = 0.0;
		cout<<"The data entered isn't valid.\r\n";
		return false;
	}
	
	//Verificamos si no es negativo 
	if(globalInputSeniorityInMonths > 0){
		cout<<"The months of seniority must exist\r\n";
		return false;
	}
	
	return true;
}
//=====================================================================================================

bool Calculate(){
	if(globalInputSeniorityInMonths < 12){
		globalUtility = 0.05 * globalInputSalary;
	}
	if(globalInputSeniorityInMonths >= 12 && globalInputSeniorityInMonths < 24){
		globalUtility = 0.07 * globalInputSalary;
	}
	if(globalInputSeniorityInMonths >= 24 && globalInputSeniorityInMonths < 60){
		globalUtility = 0.1 * globalInputSalary;
	}
	if(globalInputSeniorityInMonths >= 60 && globalInputSeniorityInMonths < 120){
		globalUtility = 0.15 * globalInputSalary;
	}
	if(globalInputSeniorityInMonths >= 120){
		globalUtility = 0.2 * globalInputSalary;
	}
	
	return true;
}
//=====================================================================================================

bool ShowResults(){
	cout<<"============Show result============\r\n";
	cout<<"\tThe profit received by the worker is: S/."<<globalUtility<<"\r\n";	
	return true;
}
