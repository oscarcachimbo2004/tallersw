/**
 * @file Exercise 07.cpp
 * @author Oscar Lopez (oscar.lopez10@unmsm.edu.pe)
 * @brief Una compa��a de alquiler de autos emite la factura de sus clientes teniendo en cuenta la distancia recorrida, 
 		  si la distancia no rebasa los 300 km., se cobra una tarifa fija de S/.250, si la distancia recorrida es mayor 
		  a 300 km. y hasta 1000 km. Se cobra la tarifa fija m�s el exceso de kil�metros a raz�n de S/.30 por km. y si la distancia recorrida es mayor a 1000 km.,  
		  la compa��a cobra la tarifa fija m�s los kms. recorridos entre 300 y 1000 a raz�n de S/. 30, y S/.20 para las distancias mayores de 1000 km. Calcular el monto que pagar� un cliente.
 * @version 1.0
 * @date 27.11.2021
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;

/*******************************************************************************************************************************************
 *  												ENUMERATIONS
 *******************************************************************************************************************************************/
float inputDistanceTraveled = 0.0;

float PaymentAmount = 0.0;
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/

void Run();
void CollectData();
void Calculate();
void ShowResults();
float VerifyPayment(float distanceTraveled);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}
//=====================================================================================================

void CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"Calculation in a course\r\n";
	cout<<"\tEnter the amount traveled in km: ";
	cin>>inputDistanceTraveled;
}
//=====================================================================================================

void Calculate(){
	PaymentAmount = VerifyPayment(inputDistanceTraveled);
}
//=====================================================================================================

void ShowResults(){
	cout<<"============Show result============\r\n";
	cout<<"The client will pay: "<<PaymentAmount<<" nuevos soles\r\n";
}
//=====================================================================================================

float VerifyPayment(float distanceTraveled){
	float fixedRate = 250.0; //Declarando datos temporales
	float paymentAmount = 0.0; //Dato que sera devuelto
	
	if((distanceTraveled>300) && (distanceTraveled<=1000)){
    
        paymentAmount =fixedRate +(distanceTraveled-300)*30;
        
    }else if(distanceTraveled > 1000){
    
        paymentAmount =fixedRate+700*30+(distanceTraveled-1000)*20;
	}else{
		paymentAmount = fixedRate;
	}
	
	return paymentAmount;
}
