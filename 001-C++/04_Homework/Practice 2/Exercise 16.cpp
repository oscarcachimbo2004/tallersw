/**
 * @file Exercise 16.cpp
 * @author Oscar Lopez (oscar.lopez10@.unmsm.edu.pe)
 * @brief La cantidad de d�as transcurridos entre dos fechas puede calcularse transform�ndolas en d�as Julianos. Esta es una 
          convenci�n astron�mica que representa cada fecha como el n�mero de d�as transcurridos desde el 1 de enero de 4713 AC. 
		  Para transformar una fecha expresada como DIA, MES y A�O en d�as Julianos se usa la siguiente f�rmula:
		  DJ = ENT (365.25 * AP) + ENT (30.6001 * MP) + DIA + 1720982
		  donde DJ es el d�a Juliano, y AP y MP son dos constantes que se obtienen como sigue:
		  Si MES = 1 � 2:  AP = A�O - 1  MP = MES + 13
		  Si MES > 2:  AP = A�O  MP = MES + 1
		  La cantidad de d�as entre dos fechas es igual a la diferencia entre los respectivos d�as Julianos:
		  d�as = (d�a Juliano 2) - (d�a Juliano 1)
		  Preparar un programa para ingresar las dos fechas como DIA1, MES1, A�O1, y DIA2, MES2, A�O2 respectivamente, y muestre 
		  la cantidad de d�as transcurridos entre ambas.
 * @version 1.8
 * @date 29.04.2022
 * 
 */

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int globalInputDay1 = 0;
int globalInputMonth1 = 0;
int globalInputYear1 = 0;
int globalInputDay2 = 0;
int globalInputMonth2 = 0;
int globalInputYear2 = 0;

int globalDJ1 = 0;
int globalDJ2 = 0;
int globalDaysDifference = 0; 
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
bool CollectData();
bool Calculate();
bool ShowResults();
int CalculateDJ(int day, int month, int year);
int DifferenceDJ(int DJ1,int DJ2);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	if(Run() == false){
		cout<<"Error running the program\r\n";
	}
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

bool Run(){
	
	while(true){
		if(CollectData() == false){
			cout<<"\r\nError requesting data, please try again\r\n";
			continue;
		}
		if(Calculate() == false){
			cout<<"\r\nError calculating, please try again\r\n";
			continue;
		}
		if(ShowResults() == false){
			cout<<"\r\nError displaying data, try again\r\n";
			continue;
		}
		break;
	}
	
	return true;
}
//=====================================================================================================

bool CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"Calculation in a course\r\n";
	cout<<"\tEnter the day in numbers: ";
	cin>>globalInputDay1;
	
	//Verificamos el dato
	if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n');
		globalInputDay1 = 0.0;
		cout<<"The day is wrong!.\r\n";
		return false;
	}
	cout<<"\tEnter the month in numbers: ";
	cin>>globalInputMonth1;
	if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n');
		globalInputMonth1 = 0.0;
		cout<<"The month is wrong!.\r\n";
		return false;
	}
	cout<<"\tEnter the year in numbers: ";
	cin>>globalInputYear1;
	if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n');
		globalInputYear1 = 0.0;
		cout<<"The year is wrong!.\r\n";
		return false;
	}
	cout<<"\tType another day: ";
	cin>>globalInputDay2;
	if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n');
		globalInputDay2 = 0.0;
		cout<<"The second day  is wrong!.\r\n";
		return false;
	}
	cout<<"\tType another month: ";
	cin>>globalInputMonth2;
	if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n');
		globalInputMonth2 = 0.0;
		cout<<"The second month is wrong!.\r\n";
		return false;
	}
	cout<<"\tType another year: ";
	cin>>globalInputYear2;
	if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n');
		globalInputMonth2 = 0.0;
		cout<<"The second month is wrong!.\r\n";
		return false;
	}
	//Verificamos si no es negativo por si hay algun gracioso
	if((globalInputDay1 <= 0) || (globalInputMonth1 <= 0) || (globalInputYear1 <= 0) || (globalInputDay2 <= 0) || (globalInputMonth2 <= 0) || (globalInputYear2 <= 0)){
		cout<<"The entered date is not valid\r\n";
		return false;
	}
	
	//Comprobamos que el a�o 1 exista 
	if(globalInputDay1 < 1||globalInputDay1 > 31||(globalInputMonth1 == 2 && globalInputDay1 > 29)||((globalInputMonth1 == 4||globalInputMonth1 == 6||globalInputMonth1 == 9||globalInputMonth1 == 11) && globalInputDay1==31)||(globalInputMonth1 == 2 && globalInputDay1 == 29 && (globalInputYear1 %4 != 0||globalInputYear1 % 100 == 0) && globalInputYear1 % 400 != 0)){
        cout<<"\r\nWrong day!\r\n"<<endl;
        return false;
    }
    if(globalInputMonth1 < 1 || globalInputMonth1 > 12){
        cout<<"\r\nWrong month!\r\n"<<endl;
        return false;
    }
    if(globalInputYear1 < 1000||globalInputYear1 >= 10000){
        cout<<"\r\nWrong year!\r\n"<<endl;
        return false;
    }
    //Comprobamos que el a�o 2 exista 
	if(globalInputDay2 < 1||globalInputDay2 > 31||(globalInputMonth2 == 2 && globalInputDay2 > 29)||((globalInputMonth2 == 4||globalInputMonth2 == 6||globalInputMonth2 == 9||globalInputMonth2 == 11) && globalInputDay2==31)||(globalInputMonth2 == 2 && globalInputDay2 == 29 && (globalInputYear2 %4 != 0||globalInputYear2 % 100 == 0) && globalInputYear2 % 400 != 0)){
        cout<<"\r\nWrong day!\r\n"<<endl;
        return false;
    }
    if(globalInputMonth2 < 1 || globalInputMonth2 > 12){
        cout<<"\r\nWrong month!\r\n"<<endl;
        return false;
    }
    if(globalInputYear2 < 1000||globalInputYear2 >= 10000){
        cout<<"\r\nWrong year!\r\n"<<endl;
        return false;
    }
    
	return true;	
}
//=====================================================================================================

bool Calculate(){
	globalDJ1 = CalculateDJ(globalInputDay1,globalInputMonth1, globalInputYear1);
	globalDJ2 = CalculateDJ(globalInputDay2,globalInputMonth2,globalInputMonth2);
	globalDaysDifference = DifferenceDJ(globalDJ1,globalDJ2);
	
	return true;	
}
//=====================================================================================================

bool ShowResults(){
	cout<<"============Show result============\r\n";
	cout<<"\tThe days between the two dates are: "<<globalDaysDifference<<" days.";
	return true;
}
//=====================================================================================================

int CalculateDJ(int day, int month, int year){
	int AP = 0;
	int MP = 0;
	
	if(month == 1 || month == 2){
		
		AP = year - 1;
		MP = month + 13;
	}
	else if(month > 2 && month <= 12){
		
		AP = year;
		MP = month + 1;
	}
	else{
		cout<<"Doesn't exist =(";
	}
	
	return ((int)(365.25 * AP) + (int)(30.6001 * MP) + day + 1720982);
}
//=====================================================================================================

int DifferenceDJ(int DJ1,int DJ2){
	return DJ2 - DJ1;
}
