/**
 * @file Template.cpp
 * @author Oscar Lopez (oscar.lopez10@unmsm.edu.pe)
 * @brief Dada la hora del d�a en horas, minutos y segundos encuentre la hora del siguiente segundo.
 * @version 2.1
 * @date 5.02.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int inputHours = 0.0;
int inputMinutes = 0.0;
int inputSeconds = 0.0;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
void Opcion(int hours,int minutes,int seconds);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}
//=====================================================================================================

void CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"\tCalculation in a course\r\n";
	cout<<"\tType the hours of the day: ";
	cin>>inputHours;
	cout<<"\tEnter the minutes of the day: ";
	cin>>inputMinutes;
	cout<<"\tType the seconds of the day: ";
	cin>>inputSeconds;
}
//=====================================================================================================

void Calculate(){
	Opcion(inputHours,inputMinutes,inputSeconds);
	inputSeconds = inputSeconds + 1;
}
//=====================================================================================================

void ShowResults(){
	cout<<"============Show result============\r\n";
	if(inputSeconds >= 60){
		inputSeconds = 0;
		inputMinutes = inputMinutes + 1;
		if(inputMinutes >= 60){
			inputMinutes = 0;
			inputHours = inputHours + 1;
			if(inputHours >= 24){
				inputHours = 0;
			}
		}
	}
	
	cout<<"\tThe given time plus one second is: "<<inputHours<<":"<<inputMinutes<<":"<<inputSeconds<<" hours\r\n";
}
//=====================================================================================================

void Opcion(int hours,int minutes,int seconds){
	int opcion = 0;
	
	cout<<"\r\nThe coordinates are:"<<"\r\n";
	cout<<"\r\n \t"<<hours<<":"<<minutes<<":"<<seconds<<" hours\r\n";
	cout<<"\r\nIt's correct?"<<"\r\n";
	cout<<"Press 1 for yes and 2 for no: ";
	cin>>opcion;
	switch(opcion){
		case 1:
			cout<<"Verified!!!"<<"\r\n"; 	
			break;
		case 2:
			cout<<"\r\nIt's incorrect!!!"<<"\r\n";
			CollectData();
			break;
		default:
			cout<<"The command isn't correct"<<"\r\n";
			Opcion(inputHours,inputMinutes,inputSeconds);
	}
}
