/**
 * @file Template.cpp
 * @author Oscar Lopez (oscar.lopez10@unmsm.edu.pe)
 * @brief Dada la ecuaci�n de la recta y = mx + c, y la ecuaci�n de la circunferencia 
		  (x-a) + (y-b)   = r  , determinar los puntos de intersecci�n de la recta con 
		  la circunferencia, y analizar si la recta es secante o tangente a la circunferencia
 * @version 1.0
 * @date 18.01.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include<iostream>
#include<math.h>
using namespace std;

/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/
float globalInputSlope = 0.0; 
float globalInputC = 0.0; //c en la ecuacion lineal
float globalInputA = 0.0; //a en la ecuacion de la circunferencia
float globalInputB = 0.0; //b en la ecuacion de la circunferencia
float globalInputRadius = 0.0;
char globalFindOut = 'V';
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
bool CollectData();
bool Calculate();
bool ShowResults();

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	if(Run() == false){
		cout<<"Error running the program\r\n";
	}
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

bool Run(){
	
	while(true){
		if(CollectData() == false){
			cout<<"\r\nError requesting data, please try again\r\n";
			continue;
		}
		if(Calculate() == false){
			cout<<"\r\nError calculating, please try again\r\n";
			continue;
		}
		if(ShowResults() == false){
			cout<<"\r\nError displaying data, try again\r\n";
			continue;
		}
		break;
	}
		
	return true;
}
//=====================================================================================================

bool CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"Calculation in a course\r\n";
	cout<<"Equation of the line: y = mx + c\r\n";
	cout<<"\tEnter the slope value: ";
	cin>>globalInputSlope;
	//Verificamos el dato
	if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n');
		cout<<"The entered value is not valid\r\n";
		return false;
	}
	cout<<"\tEnter the value of c: ";
	cin>>globalInputC;
	//Verificamos el dato
	if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n');
		cout<<"The entered value is not valid\r\n";
		return false;
	}
	cout<<"Equation of the circumference: (x-a)^ 2+ (y-b)^2   = r^2\r\n";
	cout<<"\tEnter the value of a: ";
	cin>>globalInputA;
	//Verificamos el dato
	if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n');
		cout<<"The entered value is not valid\r\n";
		return false;
	}
	cout<<"\tEnter the value of b: ";
	cin>>globalInputB;
	//Verificamos el dato
	if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n');
		cout<<"The entered value is not valid\r\n";
		return false;
	}
	cout<<"\tType the radius: ";
	cin>>globalInputRadius;	
	//Verificamos el dato
	if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n');
		cout<<"The entered value is not valid\r\n";
		return false;
	}
	
	return true;
}
//=====================================================================================================

bool Calculate(){
	if(((globalInputB - globalInputC)/(globalInputA - 0) == globalInputSlope) && (pow(globalInputA,2) + pow((globalInputC - globalInputB),2) == pow(globalInputRadius,2))){
		globalFindOut = 'V';
	}
	else{
		globalFindOut = 'F';
	}
	return true;
}
//=====================================================================================================

bool ShowResults(){
	cout<<"============Show result============\r\n";
	if(globalFindOut == 'V'){
		cout<<"\tThe line is tangent to the circle.\r\n";
	}
	else{
		cout<<"\tThe line is not tangent to the circle.\r\n";
	}
	return true;	
}
