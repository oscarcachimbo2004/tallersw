/**
 * @file Exercise 08.cpp
 * @author Oscar Lopez (oscar.lopez10@unmsm.edu.pe)
 * @brief Una empresa registra el sexo, edad y estado civil de sus empleados a trav�s de un n�mero entero positivo de cuatro cifras de 
 		  acuerdo a lo siguiente: la primera cifra de la izquierda representa el estado civil (1 para soltero, 2 para casado, 3 para viudo 
		  y 4 para divorciado), las siguientes dos cifras representan la edad y la tercera cifra representa el sexo (1 para femenino y 2 para masculino). 
		  Dise�e un programa que determine el estado civil, edad y sexo de un empleado conociendo el n�mero que empaqueta dicha informaci�n.
 * @version 1.8
 * @date 11.02.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <windows.h>
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES 
 *******************************************************************************************************************************************/
int inputCode = 0;

int age = 0;
int sex = 0;
int civilStatus = 0;
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}
//=====================================================================================================

void CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"Calculation in a course\r\n";
	cout<<"\tEnter the code: ";
	cin>>inputCode;
}
//=====================================================================================================

void Calculate(){
	civilStatus = (inputCode%10000-inputCode%1000)/1000;
	age = (inputCode%1000-inputCode%10)/10;
	sex = inputCode%10;
}
//=====================================================================================================
void ShowResults(){
	cout<<"============Show result============\r\n";
	
	if((inputCode < 10000) && (inputCode > 999)){
	
	switch(civilStatus){
		case 1:
			cout<<"\tSingle"<<"\r\n";	
			break;
		case 2:
			cout<<"\tMarried"<<"\r\n";
			break;
		case 3:
			cout<<"\tWidower"<<"\r\n";
			break;	
		case 4:
			cout<<"\tDivorced"<<"\r\n";
			break;
		default:
			system("cls");
			cout<<"INCORRECT CODE"<<"\r\n";
	   		Sleep(2000);
			system("cls");	
			Run();
		}
	   
		if(sex == 1){
			cout<<"\tFemenine"<<"\r\n";
		}else if(sex == 2){
			cout<<"\tMale"<<"\r\n";
		}else{
	   		system("cls");
			cout<<"INCORRECT CODE"<<"\r\n";
	   		Sleep(2000);
			system("cls");	
			Run();
		}
		
		cout<<"\tThe age is: "<<age<<" years old\r\n";	
	}
	
	else{	 
		system("cls");  
		cout<<"INCORRECT CODE"<<"\r\n";
		Sleep(2000);
		system("cls");	
		Run();	
	}
	
}
