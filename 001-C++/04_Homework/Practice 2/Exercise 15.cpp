/**
 * @file Exercise 15.cpp
 * @author Oscar Lopez (oscar.lopez10@unmsm.edu.pe)
 * @brief Elaborar un algoritmo en el que a partir de una fecha introducida por teclado con el formato D�a, 
 		Mes, A�o, se obtenga la fecha del d�a siguiente.
 * @version 2.0
 * @date 24.02.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int globalInputYear = 0;
int globalInputMonth = 0;
int globalInputDay = 0;
//Datos de la fecha siguiente
int globalYear = 0;
int globalMonth = 0;
int globalDay = 0;
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
bool CollectData();
bool Calculate();
bool ShowResults();
bool Opcion();

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	if(Run() == false){
		cout<<"Error running the program\r\n";
	}
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

bool Run(){
	while(true){
		if(CollectData() == false){
			cout<<"\r\nError requesting data, please try again\r\n";
			continue;
		}
		if(Calculate() == false){
			cout<<"\r\nError calculating, please try again\r\n";
			continue;
		}
		if(ShowResults() == false){
			cout<<"\r\nError displaying data, try again\r\n";
			continue;
		}
		break;
	}
	
	return true;
	
}
//=====================================================================================================

bool CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"Calculation in a course\r\nIn the form dd/mm/yyyy\r\n";
	cout<<"Type the day: ";
	cin>>globalInputDay;
	if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n');
		globalInputDay = 0.0;
		cout<<"The day is invalid.\r\n";
		return false;
	}
	cout<<"Type the month: ";
	cin>>globalInputMonth;
	if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n');
		globalInputMonth = 0;
		cout<<"The month is not valid.\r\n";
		return false;
	}
	cout<<"Type the year: ";
	cin>>globalInputYear;
	if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n');
		globalInputYear = 0.0;
		cout<<"The year is not valid.\r\n";
		return false;
	}
	
	//Verificamos si no es negativo por si hay algun gracioso
	if((globalInputDay <= 0) || (globalInputMonth <= 0) || (globalInputYear <= 0)){
		cout<<"The entered date is not valid\r\n";
		return false;
	}
	
	//Comprobamos que el a�o exista 
	if(globalInputDay < 1||globalInputDay > 31||(globalInputMonth == 2 && globalInputDay > 29)||((globalInputMonth == 4||globalInputMonth == 6||globalInputMonth == 9||globalInputMonth == 11) && globalInputDay==31)||(globalInputMonth ==2&& globalInputDay == 29 && (globalInputYear %4 != 0||globalInputYear % 100 == 0) && globalInputYear % 400 != 0)){
        cout<<"\r\nWrong day!\r\n"<<endl;
        return false;
    }
    if(globalInputMonth < 1 || globalInputMonth > 12){
        cout<<"\r\nWrong month!\r\n"<<endl;
        return false;
    }
    if(globalInputYear < 1000||globalInputYear >= 10000){
        cout<<"\r\nWrong year!\r\n"<<endl;
        return false;
    }
    
    return true;
}
//=====================================================================================================

bool Calculate(){
	Opcion();
	if((globalInputDay ==30 && (globalInputMonth == 4||globalInputMonth == 6||globalInputMonth == 9||globalInputMonth == 11))||globalInputDay == 31||(globalInputMonth ==2 && (globalInputDay == 29||(globalInputDay == 28 && (globalInputYear %4 != 0||globalInputYear %100 == 0) && globalInputYear % 400 !=0)))){
        globalDay = 1;
        globalMonth = globalInputMonth + 1;
    }
    else{
        globalDay = globalInputDay + 1;
        globalMonth =globalInputMonth;
    }
    if(globalMonth == 13){
        globalYear = globalInputYear + 1;
        globalMonth = 1;
    }
    else{
        globalYear = globalInputYear;
	}
	
	return true;
}
//=====================================================================================================

bool ShowResults(){
	cout<<"============Show result============\r\n";
	cout<<"The date the next day is....\r\n";
	cout<<globalDay<<"/"<<globalMonth<<"/"<<globalYear;
	
	return true;
}
//=====================================================================================================

bool Opcion(){
	int opcion = 0;
	
	cout<<"\r\nThe entered date is: \r\n";
	cout<<globalInputDay<<"/"<<globalInputMonth<<"/"<<globalInputYear<<"\r\n";
	cout<<"\r\nIt's correct?"<<"\r\n";
	cout<<"Press 1 for yes and 2 for no: ";
	cin>>opcion;
	if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n');
		globalInputYear = 0.0;
		cout<<"Wrong number!!.\r\n";
		Calculate();
	}
	switch(opcion){
		case 1:
			cout<<"Verified!!!"<<"\r\n"; 	
			break;
		case 2:
			cout<<"\r\nIt's incorrect!!!"<<"\r\n";
			Run();
			break;
		default:
			cout<<"The command isn't correct"<<"\r\n";
			Calculate();
			break;
	}
	
	return true;
}
