/**
 * @file Exercise 13.cpp
 * @author Oscar Lopez(oscar.lopez10@unmsm.edu.pe)
 * @brief Dado un n�mero entero; determinar si el mismo es par, impar o nulo.
 * @version 1.6
 * @date 24.02.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int globalInputNumber = 0;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
bool CollectData();
bool CheckNumber();

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	if(Run() == false){
		cout<<"Error running the program\r\n";
	}
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

bool Run(){
	while(true){
		if(CollectData() == false){
			cout<<"\r\nError requesting data, please try again\r\n";
			continue;
		}
		if(CheckNumber() == false){
			cout<<"\r\nError checking number, try again\r\n";
			continue;
		}
		break;
	}
}
//=====================================================================================================

bool CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"Calculation in a course\r\n";
	cout<<"\tEnter the number to evaluate: ";
	cin>>globalInputNumber;
	//Verificamos el dato
	if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n');
		globalInputNumber = 0.0;
		cout<<"You have not entered a correct number.\r\n";
		return false;
	}
	//Verificamos si no es negativo por si hay algun gracioso
	if(globalInputNumber < 0){
		cout<<"The evaluated number cannot be negative.\r\n";
		return false;
	}
	
}
//=====================================================================================================

bool CheckNumber(){
	if(globalInputNumber == 0){
		cout<<"\tThe number is null\r\n";
	}
	else if(globalInputNumber % 2 == 0){
		cout<<"\tThe number is even\r\n";
	}
	else{
		cout<<"\tThe number is odd\r\n";
	}
}
