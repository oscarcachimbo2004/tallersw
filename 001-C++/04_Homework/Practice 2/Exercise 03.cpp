/**
 * @file Exercise 03.cpp
 * @author Oscar Lopez (oscar.lopez10@unmsm.edu.pe)
 * @brief Ordene de mayor a menor 3 n�meros  ingresados por teclado
 * @version 2.6
 * @date 05.01.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;

/*******************************************************************************************************************************************
 *  											GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float number1 = 0.0;
float number2 = 0.0;
float number3 = 0.0;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void CheckOrden();

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
	CollectData();
	CheckOrden();
}
//=====================================================================================================

void CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"Calculation in a course\r\n";
	cout<<"Enter the number 1: ";
	cin>>number1;
	cout<<"Enter the number 2: ";
	cin>>number2;
	cout<<"Enter the number 3: ";
	cin>>number3;
}

//=====================================================================================================

void CheckOrden(){
	
	cout<<"============Show results============\r\n";
	if(number1>number2 && number2>number3){
		cout<<"The correct order is: "<<number1<<" "<<number2<<" "<<number3;
	}else if(number1>number3 && number3>number2){
		cout<<"The correct order is: "<<number1<<" "<<number3<<" "<<number2;
	}else if(number2>number1 && number1>number3){
		cout<<"The correct order is: "<<number2<<" "<<number1<<" "<<number3;
	}else if(number2>number3 && number3>number1){
		cout<<"The correct order is: "<<number2<<" "<<number3<<" "<<number1;
	}else if(number3>number1 && number1>number2){
		cout<<"The correct order is: "<<number3<<" "<<number1<<" "<<number2;
	}else if(number3>number2 && number2>number1){
		cout<<"The correct order is: "<<number3<<" "<<number2<<" "<<number1;
	}else{
		cout<<"At least one pair are the same....\r\nInsert again\r\n";
		CollectData();
	}
	
}
