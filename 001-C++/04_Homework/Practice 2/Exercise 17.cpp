/**
 * @file Exercise 17.cpp
 * @author Oscar Lopez (oscar.lopez10@unmsm.edu.pe)
 * @brief Determinar la cantidad de dinero que recibir� un trabajador por concepto de las horas extras trabajadas en una empresa, 
 		  sabiendo que cuando las horas de trabajo exceden de 40, el resto se consideran horas extras y que estas se pagan al doble 
		  de una hora normal cuando no exceden de 8; si las horas extras exceden de 8 se pagan las primeras 8 al doble de lo que 
		  se pagan las horas normales y el resto al triple.
 * @version 1.0
 * @date 30.04.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <stdlib.h>
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float globalPayPerHour = 0.0;
float globalHoursWorked = 0.0;

float globalOvertimePay = 0.0;
float globalExtraHours = 0.0;
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
bool CollectData();
bool Calculate();
bool ShowResults();

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	if(Run() == false){
		cout<<"Error running the program\r\n";
	}
	
	return 0;
}
/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

bool Run(){
	while(true){
		if(CollectData() == false){
			cout<<"\r\nError requesting data, please try again\r\n";
			continue;
		}
		if(Calculate() == false){
			cout<<"\r\nError calculating, please try again\r\n";
			continue;
		}
		if(ShowResults() == false){
			cout<<"\r\nError displaying data, try again\r\n";
			continue;
		}
		break;
	}
	
	return true;
}
//=====================================================================================================

bool CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"Calculation in a course\r\n";
	cout<<"\tEnter the hours worked: ";
	cin>>globalHoursWorked;
	cout<<"\tEnter hourly pay: ";
	cin>>globalPayPerHour;
	
	//Verificamos el dato
	if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n');
		globalHoursWorked = 0.0;
		cout<<"Isn't valid.\r\n";
		return false;
	}
	
	if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n');
		globalPayPerHour = 0.0;
		cout<<"Isn't valid.\r\n";
		return false;
	}
	
	return true;
}
//=====================================================================================================

bool Calculate(){
	if(globalHoursWorked > 40){
		globalExtraHours = globalHoursWorked - 40;
		if(globalExtraHours <= 8){
			globalOvertimePay = globalExtraHours * 2 * globalPayPerHour;
		}
		else{
			globalOvertimePay = 8 * 2 * globalPayPerHour + (globalExtraHours - 8) * 3 * globalPayPerHour;
		}
	} else{
		cout<<"\tThere is no payment for overtime!!";
		
		exit(0);
	}
	
	return true;
}
//=====================================================================================================

bool ShowResults(){
	cout<<"============Show result============\r\n";
	cout<<"\tOvertime pay is: "<<globalOvertimePay<<" bolivares\r\n";
	
	return true;
}

