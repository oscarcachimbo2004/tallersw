/**
 * @file Exercise 04.cpp
 * @author Oscar Lopez (oscar.lopez10@unmsm.edu.pe)
 * @brief Dado un tiempo en minutos, calcular los d�as, horas y minutos que le corresponden
 * @version 1.9
 * @date 5.01.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES 
 *******************************************************************************************************************************************/
int inputTotalMinutes = 0;

int days = 0;
int hours = 0;
int minutes = 0;
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}
//=====================================================================================================

void CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"Calculation in a course\r\n";
	cout<<"Enter the total time in minutes: ";
	cin>>inputTotalMinutes;
}
//=====================================================================================================

void Calculate(){
	
	if(inputTotalMinutes%1440==0){
		days = inputTotalMinutes/1440;
	}else{
		days = inputTotalMinutes/1440;
		inputTotalMinutes = inputTotalMinutes%1440;
	}
	if(inputTotalMinutes%60==0){
		hours = inputTotalMinutes/60;
	}else{
		hours = inputTotalMinutes/60;
		minutes = inputTotalMinutes%60;
	}
}
//=====================================================================================================

void ShowResults(){
	cout<<"============Show result============\r\n";
	cout<<"The time conversion is: "<<days<<" days "<<hours<<" hours "<<minutes<<" minutes";
}
//=====================================================================================================

