/**
 * @file Exercise 11.cpp
 * @author Oscar Lopez(oscar.lopez10@unmsm.edu.pe)
 * @brief Se necesita un sistema para un supermercado, en el cual si el monto de la compra del cliente es mayor de $5000 se le har� un 
		descuento del 30%, si es menor o igual a $5000 pero mayor que $3000 ser� del 20%, si no rebasa los $3000 pero si los $1000 la 
		rebaja efectiva es del 10% y en caso de que no rebase los $1000 no tendr� beneficio.
 * @version 1.8
 * @date 24.02.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float globalInputPurchaseAmount = 0.0;

float globalDiscount = 0.0;
float globalTotalToPay = 0.0;
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
bool CollectData();
bool Calculate();
bool ShowResults();

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	if(Run() == false){
		cout<<"Error running the program\r\n";
	}
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

bool Run(){
	while(true){
		if(CollectData() == false){
			cout<<"\r\nError requesting data, please try again\r\n";
			continue;
		}
		if(Calculate() == false){
			cout<<"\r\nError calculating, please try again\r\n";
			continue;
		}
		if(ShowResults() == false){
			cout<<"\r\nError displaying data, try again\r\n";
			continue;
		}
		break;
	}
	
	return true;
}
//=====================================================================================================

bool CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"Calculation in a course\r\n";
	cout<<"\tEnter the purchase amount: ";
	cin>>globalInputPurchaseAmount;
	
	//Verificamos el dato
	if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n');
		globalInputPurchaseAmount = 0.0;
		cout<<"\tThe amount entered is not valid.\r\n";
		return false;
	}
	
	//Verificamos si no es negativo por si hay algun gracioso
	if(globalInputPurchaseAmount <= 0){
		cout<<"\tThe amount entered is not valid, it cannot be negative\r\n";
		return false;
	}
	
	return true;
}
//=====================================================================================================

bool Calculate(){
	if(globalInputPurchaseAmount > 5000){
		globalDiscount = globalInputPurchaseAmount * 0.3;
		globalTotalToPay = globalInputPurchaseAmount - globalDiscount;
	}
	if((globalInputPurchaseAmount <= 5000) && (globalInputPurchaseAmount > 3000)){
		globalDiscount = globalInputPurchaseAmount * 0.2;
		globalTotalToPay = globalInputPurchaseAmount - globalDiscount;
	}
	if((globalInputPurchaseAmount <= 3000) && (globalInputPurchaseAmount > 1000)){
		globalDiscount = globalInputPurchaseAmount * 0.2;
		globalTotalToPay = globalInputPurchaseAmount - globalDiscount;
	}
	
	return true;	
}
//=====================================================================================================

bool ShowResults(){
	cout<<"============Show result============\r\n";
	cout<<"\tThe value of the discount is: $"<<globalDiscount<<"\r\n";
	cout<<"\tThe amount to be paid by the user is: $"<<globalTotalToPay<<"\r\n";
	
	return true;
}
