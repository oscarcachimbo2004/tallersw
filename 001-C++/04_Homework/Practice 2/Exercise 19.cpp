/**
 * @file Exercise 19.cpp
 * @author Oscar Lopez (oscar.lopez10@unmsm.edu.pe)
 * @brief Ingrese seis notas y calcule el promedio, considerando las 5 mejores notas. 
 * @version 1.0
 * @date 30.04.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float globalFirstNote = 0.0;
float globalSecondNote = 0.0;
float globalThirdNote = 0.0;
float globalQuaterNote = 0.0;
float globalFifthNote = 0.0;
float globalSixthNote = 0.0;

float globalAverage = 0.0;
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
bool CollectData();
bool Calculate();
bool ShowResults();

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	if(Run() == false){
		cout<<"Error running the program\r\n";
	}
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

bool Run(){
	
	while(true){
		if(CollectData() == false){
			cout<<"\r\nError requesting data, please try again\r\n";
			continue;
		}
		if(Calculate() == false){
			cout<<"\r\nError calculating, please try again\r\n";
			continue;
		}
		if(ShowResults() == false){
			cout<<"\r\nError displaying data, try again\r\n";
			continue;
		}
		break;
	}
	
	return true;
}
//=====================================================================================================

bool CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"Calculation in a course\r\n";
	cout<<"\tType the first note: ";
	cin>>globalFirstNote;
	//Verificamos el dato
	if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n');
		globalFirstNote = 0.0;
		cout<<"First note isn't valid.\r\n";
		return false;
	}
	cout<<"\tType the second note: ";
	cin>>globalSecondNote;
	//Verificamos el dato
	if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n');
		globalSecondNote = 0.0;
		cout<<"Second note isn't valid.\r\n";
		return false;
	}
	cout<<"\tType the third note: ";
	cin>>globalThirdNote;
	//Verificamos el dato
	if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n');
		globalThirdNote = 0.0;
		cout<<"Third note isn't valid.\r\n";
		return false;
	}
	cout<<"\tType the quarter note: ";
	cin>>globalQuaterNote;
	//Verificamos el dato
	if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n');
		globalQuaterNote = 0.0;
		cout<<"Quarter note isn't valid.\r\n";
		return false;
	}
	cout<<"\tType the fifth note: ";
	cin>>globalFifthNote;
	//Verificamos el dato
	if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n');
		globalFifthNote = 0.0;
		cout<<"Fifth note isn't valid.\r\n";
		return false;
	}	
	cout<<"\tType the sixth note: ";
	cin>>globalSixthNote;
	//Verificamos el dato
	if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n');
		globalSixthNote = 0.0;
		cout<<"Sixth note isn't valid.\r\n";
		return false;
	}
	
	return true;
}
//=====================================================================================================

bool Calculate(){
	
	if((globalFirstNote < globalSecondNote) && (globalFirstNote < globalThirdNote) && (globalFirstNote < globalQuaterNote) && (globalFirstNote < globalFifthNote) && (globalFirstNote < globalSixthNote)){
		globalAverage = (globalSecondNote + globalThirdNote + globalQuaterNote + globalFifthNote + globalSixthNote)/ 5.0;	
	}
	else if((globalSecondNote < globalFirstNote) && (globalSecondNote < globalThirdNote) && (globalSecondNote < globalQuaterNote) && (globalSecondNote < globalFifthNote) && (globalSecondNote < globalSixthNote)){
		globalAverage = (globalFirstNote + globalThirdNote + globalQuaterNote + globalFifthNote + globalSixthNote)/ 5.0;
	}
	else if((globalThirdNote < globalFirstNote) && (globalThirdNote < globalSecondNote) && (globalThirdNote < globalQuaterNote) && (globalThirdNote < globalFifthNote) && (globalThirdNote < globalSixthNote)){
		globalAverage = (globalFirstNote + globalSecondNote + globalQuaterNote + globalFifthNote + globalSixthNote)/ 5.0;
	}
	else if((globalQuaterNote < globalFirstNote) && (globalQuaterNote < globalSecondNote) && (globalQuaterNote < globalThirdNote) && (globalQuaterNote < globalFifthNote) && (globalQuaterNote < globalSixthNote)){
		globalAverage = (globalFirstNote + globalSecondNote + globalThirdNote + globalFifthNote + globalSixthNote)/ 5.0;
	}	
	else if((globalFifthNote < globalFirstNote) && (globalFifthNote < globalSecondNote) && (globalFifthNote < globalThirdNote) && (globalFifthNote < globalQuaterNote) && (globalFifthNote < globalSixthNote)){
		globalAverage = (globalFirstNote + globalSecondNote + globalThirdNote + globalQuaterNote + globalSixthNote)/ 5.0;
	}
	else if((globalSixthNote < globalFirstNote) && (globalSixthNote < globalSecondNote) && (globalSixthNote < globalThirdNote) && (globalSixthNote < globalQuaterNote) && (globalSixthNote < globalFifthNote)){
		globalAverage = (globalFirstNote + globalSecondNote + globalThirdNote + globalQuaterNote + globalFifthNote)/ 5.0;
	}	
	return true;
}
//=====================================================================================================

bool ShowResults(){
	cout<<"============Show result============\r\n";
	cout<<"The average of the top five grades is: "<<globalAverage<<"\r\n";
	return true;
}
