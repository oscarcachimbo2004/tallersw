/**
 * @file Exercise 14.cpp
 * @author Oscar Lopez (oscar.lopez10@unmsm.edu.pe)
 * @brief Escribir un programa que determine si un a�o es bisiesto. Un a�o es bisiesto si es m�ltiplo de 4 (por ejemplo 1984). 
 		Los a�os m�ltiplos de 100 no son bisiestos, salvo si ellos son tambi�n m�ltiplos de 400 (2000 es bisiesto, pero;  1800 no lo es)
 * @version 1.8
 * @date 27.11.2021
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBLA VARIABLES
 *******************************************************************************************************************************************/
int globalInputYear = 0;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
bool CollectData();
bool ReviewYear();

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	if(Run() == false){
		cout<<"Error running the program\r\n";	
	}
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

bool Run(){
	while(true){
		if(CollectData() == false){
			cout<<"\r\nError requesting data, please try again\r\n";
			continue;
		}
		if(ReviewYear() == false){
			cout<<"\r\nError checking year, try again\r\n";
			continue;
		}
		break;
	}
	
	return true;
}
//=====================================================================================================

bool CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"Calculation in a course\r\n";
	cout<<"Enter the year to review: ";
	cin>>globalInputYear;
	//Verificamos el dato
	if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n');
		globalInputYear = 0;
		cout<<"The year entered isn't valid.\r\n";
		return false;
	}
	//Verificamos si no es negativo por si hay algun gracioso
	if(globalInputYear < 0){
		cout<<"The amount entered is not valid, it cannot be negative\r\n";
		return false;
	}
	return true;
}
//=====================================================================================================

bool ReviewYear(){
	if((globalInputYear % 4 != 0) || ((globalInputYear % 100 == 0) && (globalInputYear % 400 !=0))){
		cout<<"\r\nThe entered year is leap year\r\n";
	}
	else{
		cout<<"\r\nThe year entered isn't a leap year\r\n";
	}
	
	return true;
}
