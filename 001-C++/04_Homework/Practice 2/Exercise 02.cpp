/**
 * @file Template.cpp
 * @author Oscar Lopez (oscar.lopez10@unmsm.edu.pe)
 * @brief A un trabajador le descuentan de su sueldo el 10% si su sueldo es menor o igual a 1000, por encima de 1000 hasta 2000 el 5% del 
 		  adicional, y por encima de 2000 el 3% del adicional. Calcular el descuento y sueldo neto que recibe el trabajador dado un sueldo.
 * @version 1.8
 * @date 3.02.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float inputSalary = 0.0;
												
float discount = 0.0;
float totalSalary = 0.0; 
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
float CalculateDiscount(float salary);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	Run();
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}
//=====================================================================================================

void CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"Calculation in a course\r\n";
	cout<<"Insert your Latin American salary: ";
	cin>>inputSalary;
}
//=====================================================================================================

void Calculate(){
	discount = CalculateDiscount(inputSalary);
	totalSalary = inputSalary - discount;
}
//=====================================================================================================

void ShowResults(){
	cout<<"============Show result============\r\n";
	cout<<"\tThe discounted Latin American salary is: "<<totalSalary<<" Bolivares\r\n";
}
//=====================================================================================================

float CalculateDiscount(float salary){
	float discount1 = 0.0;
	
	if(salary <= 1000){
		discount = salary*10.0/100.0;
	}else if(salary > 1000 && salary <=2000){
		discount = (salary*10.0/100.0) * 5.0/100.0;
	}else{
		discount = ((salary*10.0/100.0) * 5.0/100.0) * 3.0/100.0;
	}
	
	return discount;
}
