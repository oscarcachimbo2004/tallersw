/**
 * @file Exercise 4.cpp
 * @author Oscar Lopez (oscar.lopez10@unmsm.edu.pe)
 * @brief Escribir un algoritmo que determine si la cadena abc aparece en una secuencia de caracteres cuyo final viene dado por un punto.
 * @version 1.0
 * @date 02.05.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
char globalInputCharacteres; 
bool globalValidate = false;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
bool CollectData();
bool Calculate();
bool ShowResults();

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	if(Run() == false){
		cout<<"Error running the program\r\n";
	}
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

bool Run(){
	while(true){
		if(CollectData() == false){
			cout<<"\r\nError requesting data, please try again\r\n";
			continue;
		}
		if(ShowResults() == false){
			cout<<"\r\nError displaying data, try again\r\n";
			continue;
		}
		break;
	}
	
	return true;
}
//=====================================================================================================

bool CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"Calculation in a course\r\n";
	cout<<"Type a sequence of characters ending in '.': ";
	cin>>globalInputCharacteres;
	
	return true;
}
//=====================================================================================================

bool ShowResults(){
	
	do{
		if(globalInputCharacteres=='a'){
			cin>>globalInputCharacteres;
			if(globalInputCharacteres=='b'){
				cin>>globalInputCharacteres;
				if(globalInputCharacteres=='c'){
					globalValidate = true;
					cin>>globalInputCharacteres;
				}
			}
		}else{
			cin>>globalInputCharacteres;
		}
	}while(globalInputCharacteres!='.');
 
	if(globalValidate){
		cout <<"The string 'abc' is found in the sequence of characters.";
	}else{
		cout << "The string 'abc' is not found in the character string.";
	}
	
	return true;
}
