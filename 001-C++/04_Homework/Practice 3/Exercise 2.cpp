/**
 * @file Exercise 2.cpp
 * @author Oscar Lopez (oscar.lopez10@unmsm.edu.pe)
 * @brief File description
 * @version 1.0
 * @date 2.05.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <stdlib.h>
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int globalInputNumber = 0; 

int globalNumberOfDigits = 0;
int globalSumOfEvenDigitNumbers = 0;
int globalSumOfOddDigitNumbers = 0;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
bool CollectData();
bool Calculate();
bool ShowResults();
int NumberOfDigits(int number);
int SumOfEvenDigitNumbers(int number);
int SumOfOddDigitNumbers(int number);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	if(Run() == false){
		cout<<"Error running the program\r\n";
	}
	system("pause");
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

bool Run(){
	while(true){
		if(CollectData() == false){
			cout<<"\r\nError requesting data, please try again\r\n";
			continue;
		}
		if(Calculate() == false){
			cout<<"\r\nError calculating, please try again\r\n";
			continue;
		}
		if(ShowResults() == false){
			cout<<"\r\nError displaying data, try again\r\n";
			continue;
		}
		break;
	}
	
	return true;
}
//=====================================================================================================

bool CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"Calculation in a course\r\n";
	cout<<"\tEnter a number: ";
	cin>>globalInputNumber;
	//Verificamos el dato
	if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n');
		globalInputNumber = 0.0;
		cout<<"Isn't valid.\r\n";
		return false;
	}
	//Verificamos si no es negativo 
	if(globalInputNumber < 0){
		cout<<"The number must exist\r\n";
		return false;
	}
	
	return true;
}
//=====================================================================================================

bool Calculate(){
	globalNumberOfDigits = NumberOfDigits(globalInputNumber);
	globalSumOfEvenDigitNumbers = SumOfEvenDigitNumbers(globalInputNumber);
	globalSumOfOddDigitNumbers = SumOfOddDigitNumbers(globalInputNumber);
	
	return true;
}
//=====================================================================================================

bool ShowResults(){
	cout<<"============Show result============\r\n";
	cout<<"\tThe number of digits in the number is: "<<globalNumberOfDigits<<"\r\n";
	cout<<"\tThe sum of the even digits of the number is: "<<globalSumOfEvenDigitNumbers<<"\r\n";
	cout<<"\tThe sum of the odd digits of the number is: "<<globalSumOfOddDigitNumbers<<"\r\n";
	
	return true;
}
//=====================================================================================================

int NumberOfDigits(int number){
	int numberOfDigits = 1;
	
	while(number >= 10){
		number = number/10;
		numberOfDigits ++;
	}
	
	return numberOfDigits;
}
//=====================================================================================================

int SumOfEvenDigitNumbers(int number){
	int sumOfEvenDigitNumbers = 0;
	
	while(number > 0){
		if((number%10)%2==0){
			sumOfEvenDigitNumbers += number % 10;
		}
		number /= 10;
	}
	
	return sumOfEvenDigitNumbers;
}
//=====================================================================================================

int SumOfOddDigitNumbers(int number){
	int sumOfOddDigitNumbers = 0;
		
	while(number > 0){
		if((number%10)%2!=0){
			sumOfOddDigitNumbers += number % 10;
		}
		number /= 10;
	}
	return sumOfOddDigitNumbers;
}
