/**
 * @file Exercise 7.cpp
 * @author Oscar Lopez (oscar.lopez10@unmsm.edu.pe)
 * @brief En un centro meteorol�gico se llevan los promedios mensuales de las lluvias ca�das en las principales regiones del pa�s. 
 		  Existen 3 regiones importantes. NORTE, CENTRO y SUR. Escriba un algoritmo para calcular lo siguiente:
 		  	El promedio anual de la regi�n centro.
			El mes y regi�n con menor lluvia en la regi�n sur.
			La regi�n con mayor lluvia anual.
 * @version 1.0
 * @date 2.05.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <stdlib.h>
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int globalInputRainsInTheMonth = 0;
int globalInputRegionNumber = 0;

int globalCounter = 0;
int globalAverageAnnualRainfallInTheCenter = 0;
int globalLeastRainyMonthInTheSouth = 9999999;
int globalHighestAnnualRainfallInTheCentral = 0;
int globalHighestAnnualRainfallInTheSouth = 0;
int globalHighestAnnualRainfallInTheNorth = 0;
int globalLargestRegionWithAnnualRainfall = 0;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
bool CollectData();
bool Calculate();
bool ShowResults();
bool Option();

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	if(Run() == false){
		cout<<"Error running the program\r\n";
	}
	system("pause");
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

bool Run(){
	while(true){
		if(CollectData() == false){
			cout<<"\r\nError requesting data, please try again\r\n";
			continue;
		}
		if(Calculate() == false){
			cout<<"\r\nError calculating, please try again\r\n";
			continue;
		}
		if(Option() == false){
			cout<<"\r\nError opcion, please try again\r\n";
			continue;
		}
		if(ShowResults() == false){
			cout<<"\r\nError displaying data, try again\r\n";
			continue;
		}
		break;
	}
	
	return true;
}
//=====================================================================================================

bool CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"Calculation in a course\r\n";
		cout<<"\t \r\nChoose the region: \r\n";
		cout<<"\t\t1.North\r\n";
		cout<<"\t\t2.Central\r\n";
		cout<<"\t\t3.South\r\n";
		cout<<"\t\t: ";
		
			cin>>globalInputRegionNumber;
			if(cin.fail() == true){
				cin.clear();
				cin.ignore(1000,'\n');
				globalInputRegionNumber = 0.0;
				cout<<"Isn't valid.\r\n";
				return false;
			}
			if(globalInputRegionNumber<1 || globalInputRegionNumber>3){
				cout<<"\tWrong value.\r\nTry Again!!";
			}
			if(globalInputRegionNumber == 2){
				for(int i=1;i<=12;i++){
					cout<<"\tEnter the number of days it rained in the month "<<i<<" is:";
					cin>>globalInputRainsInTheMonth;
					//Verificamos el dato
					if(cin.fail() == true){
						cin.clear();
						cin.ignore(1000,'\n');
						globalInputRainsInTheMonth = 0.0;
						cout<<"Isn't valid.\r\n";
						return false;
					}
					globalAverageAnnualRainfallInTheCenter += globalInputRainsInTheMonth;
					if(globalInputRainsInTheMonth>globalHighestAnnualRainfallInTheCentral){
						globalHighestAnnualRainfallInTheCentral = globalInputRainsInTheMonth;
					}
				}
			}
			if(globalInputRegionNumber == 3){
				for(int i=1;i<=12;i++){
					cout<<"\tEnter the number of days it rained in the month "<<i<<" is:";
					cin>>globalInputRainsInTheMonth;
					//Verificamos el dato
					if(cin.fail() == true){
						cin.clear();
						cin.ignore(1000,'\n');
						globalInputRainsInTheMonth = 0.0;
						cout<<"Isn't valid.\r\n";
						return false;
					}
					if(globalInputRainsInTheMonth<globalLeastRainyMonthInTheSouth){
						globalLeastRainyMonthInTheSouth = globalInputRainsInTheMonth;
					}
					if(globalInputRainsInTheMonth>globalHighestAnnualRainfallInTheSouth){
						globalHighestAnnualRainfallInTheSouth = globalInputRainsInTheMonth;
					}
				}
			}
			if(globalInputRegionNumber == 1){
				for(int i=1;i<=12;i++){
					cout<<"\tEnter the number of days it rained in the month "<<i<<" is:";
					cin>>globalInputRainsInTheMonth;
					//Verificamos el dato
					if(cin.fail() == true){
						cin.clear();
						cin.ignore(1000,'\n');
						globalInputRainsInTheMonth = 0.0;
						cout<<"Isn't valid.\r\n";
						return false;
					}
					if(globalInputRainsInTheMonth>globalHighestAnnualRainfallInTheNorth){
						globalHighestAnnualRainfallInTheNorth = globalInputRainsInTheMonth;
					}	
				}
			}
			

	
	return true;	
	}
//=====================================================================================================

bool Calculate(){
	
	globalAverageAnnualRainfallInTheCenter /= 12;
	if(globalHighestAnnualRainfallInTheCentral > globalHighestAnnualRainfallInTheSouth){
		if(globalHighestAnnualRainfallInTheCentral > globalHighestAnnualRainfallInTheNorth){
			globalLargestRegionWithAnnualRainfall = globalHighestAnnualRainfallInTheCentral;
		}
		else{
			globalLargestRegionWithAnnualRainfall = globalHighestAnnualRainfallInTheNorth;
		}
	}
	else if(globalHighestAnnualRainfallInTheSouth > globalHighestAnnualRainfallInTheNorth){
		globalLargestRegionWithAnnualRainfall = globalHighestAnnualRainfallInTheSouth;
	}
	else{
		globalLargestRegionWithAnnualRainfall = globalHighestAnnualRainfallInTheNorth;
	}
	return true;
}
//=====================================================================================================

bool ShowResults(){
	cout<<"\r\n============Show result============\r\n";
	cout<<"\tThe average for the central region is: "<<globalAverageAnnualRainfallInTheCenter<<" degrees\r\n";
	cout<<"\tThe lowest temperature in a month in the southern region is: "<<globalLeastRainyMonthInTheSouth<<" degrees\r\n";
	if(globalLargestRegionWithAnnualRainfall == globalHighestAnnualRainfallInTheCentral){
		cout<<"\tThe region with the highest degree is the one in the center and is: "<<globalLargestRegionWithAnnualRainfall<<" degrees\r\n";
	}
	if(globalLargestRegionWithAnnualRainfall == globalHighestAnnualRainfallInTheNorth){
		cout<<"\tThe region with the highest degree is the one in the north and is: "<<globalLargestRegionWithAnnualRainfall<<" degrees\r\n";
	}
	if(globalLargestRegionWithAnnualRainfall == globalHighestAnnualRainfallInTheSouth){
		cout<<"\tThe region with the highest degree is the one in the south and is: "<<globalLargestRegionWithAnnualRainfall<<" degrees\r\n";
	}
	
	return true;
}
//=====================================================================================================

bool Option(){
	int opcion;
	
	cout<<"\tDo you want to continue entering data?"<<"\r\n";
	cout<<"\t1.Yes\r\n";
	cout<<"\t2.No\r\n";
	cout<<"\t\t: ";
	cin>>opcion;
	
	switch(opcion){
		case 1:
			cout<<"\tGOOD!!"<<"\r\n";
			CollectData();
			break;
		case 2: 
			cout<<"\tGOOD BYE!!"<<"\r\n";
			exit(0);
			break;
		default:
			cout<<"\tInvalid option"<<"\r\n";
			break;
	}
	
}
