/**
 * @file Exercise 3.cpp
 * @author Oscar Lopez (oscar.lopez10@unmsm.edu.pe)
 * @brief Determinar la suma de los N primeros t�rminos de la siguiente serie: 
 		        1+X+X^2/2!+X^3/3!+X^4/4!+........
 * @version 1.0
 * @date 02.05.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>
#include <stdlib.h>
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int globalNumberOfTermsInTheSeries = 0;
int globalSeriesValue = 0;

int globalFactorial = 1;
float globalSeriesResult = 0.0;
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
bool CollectData();
bool Calculate();
bool ShowResults();

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	if(Run() == false){
		cout<<"Error running the program\r\n";
	}
	system("pause");
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

bool Run(){
	while(true){
		if(CollectData() == false){
			cout<<"\r\nError requesting data, please try again\r\n";
			continue;
		}
		if(Calculate() == false){
			cout<<"\r\nError calculating, please try again\r\n";
			continue;
		}
		if(ShowResults() == false){
			cout<<"\r\nError displaying data, try again\r\n";
			continue;
		}
		break;
	}
	
	return true;
}
//=====================================================================================================

bool CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"Calculation in a course\r\n";
	cout<<"\tThe sequence has the form: \r\n";
	cout<<"\t\t1+X+X^2/2!+X^3/3!+X^4/4!+........\r\n";
	cout<<"\r\n \tType the number of terms of the equation: ";
	cin>>globalNumberOfTermsInTheSeries;
	//Verificamos el dato
	if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n');
		globalNumberOfTermsInTheSeries = 0.0;
		cout<<"Number isn't valid.\r\n";
		return false;
	}
	//Verificamos si no es negativo 
	if(globalNumberOfTermsInTheSeries < 0){
		cout<<"The number must exist\r\n";
		return false;
	}
	cout<<"\tType the value of the X: ";
	cin>>globalSeriesValue;
	//Verificamos el dato
	if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n');
		globalSeriesValue = 0.0;
		cout<<"Number isn't valid.\r\n";
		return false;
	}
	//Verificamos si no es negativo 
	if(globalSeriesValue < 0){
		cout<<"The number of serie must exist\r\n";
		return false;
	}
	return true;
}
//=====================================================================================================

bool Calculate(){
	for(int i=1;i<=globalNumberOfTermsInTheSeries - 1;i++){
		globalFactorial *= i;
		
		globalSeriesResult += pow(globalSeriesValue,i)/globalFactorial;
	}
	globalSeriesResult ++;
	
	return true;
}
//=====================================================================================================

bool ShowResults(){
	cout<<"============Show result============\r\n";
	cout<<"\tThe value of the series of "<<globalNumberOfTermsInTheSeries<<" elements is: "<<globalSeriesResult<<"\r\n";
	
	return true;
}
