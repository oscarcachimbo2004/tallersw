/**
 * @file Exercise 5.cpp
 * @author Oscar Lopez (oscar.lopez10@unmsm.edu.pe)
 * @brief A una fiesta asistieron personas de diferentes edades y sexos. Construir un algoritmos dadas las edades y sexos de las personas
 		  Calcular : 
 		  -Cu�ntas personas asistieron a la fiesta 
 		  -Cu�ntos hombres y cuantas mujeres
 		  -Promedio de edades por sexo
 		  -La edad de la persona m�s joven que asisti�
 		  -No se permiten menores de edad a la fiesta 
 		  -Ingresar datos hasta que se ingrese una edad a cero
 * @version 1.0
 * @date 02.05.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <stdlib.h>
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int globalInputGender = 0;
int globalInputAge = 0;

int globalAssistants = 0;
int globalNumberOfMen = 0;
int globalNumberOfWomen = 0;
int globalAverageAgeMen = 0;
int globalAverageAgeWomen = 0;
int globalAgeOfYoungestPerson = 0;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
bool CollectData();
bool Calculate();
bool ShowResults();

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	if(Run() == false){
		cout<<"Error running the program\r\n";
	}
	system("pause");
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

bool Run(){
	while(true){
		if(CollectData() == false){
			cout<<"\r\nError requesting data, please try again\r\n";
			continue;
		}
		if(Calculate() == false){
			cout<<"\r\nError calculating, please try again\r\n";
			continue;
		}
		if(ShowResults() == false){
			cout<<"\r\nError displaying data, try again\r\n";
			continue;
		}
		break;
	}
	
	return true;
}
//=====================================================================================================

bool CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"Calculation in a course\r\n";
	do{
		cout<<"\tEnter the age value: ";
		cin>>globalInputAge;
		//Verificamos el dato
		if(cin.fail() == true){
			cin.clear();
			cin.ignore(1000,'\n');
			globalInputAge = 0;
			cout<<"Age isn't valid.\r\n";
			return false;
		}
		cout<<"\t \r\nSelect gender value: \r\n";
		cout<<"\t\t1.Women\r\n";
		cout<<"\t\t2.Men\r\n";
		cout<<"\t\t: ";		
		do{
			cin>>globalInputGender;
			if(globalInputGender<1||globalInputGender>2){
				cout<<"\tWrong value.\r\nTry Again!!";
			}
		}while(globalInputGender<1||globalInputGender>2);
		if(globalInputAge>=18){
				globalAssistants += 1;
		}
		else{
			cout<<"\tYou are underage./r/nYou have to change your diaper\r\n";
		}
		if(globalInputGender==1 && globalInputAge>=18){
			globalNumberOfWomen += 1;
			globalAverageAgeWomen += globalInputAge;
		}
		if(globalInputGender==2 && globalInputAge>=18){
			globalNumberOfMen += 1;
			globalAverageAgeMen += globalInputAge;
		}
		if(globalInputAge>=18 && (globalAgeOfYoungestPerson==0||globalAgeOfYoungestPerson>globalInputAge)){
			globalAgeOfYoungestPerson = globalInputAge;
		}	
		cout<<"\r\n";
	}while(globalInputAge !=0);
	
	return true;
}
//=====================================================================================================

bool Calculate(){
	if(globalNumberOfMen == 0){
		globalAverageAgeMen = 0;
	}
	else{
		globalAverageAgeMen /= globalNumberOfMen;
	}
	if(globalNumberOfWomen == 0){
		globalAverageAgeWomen = 0;
	}
	else{
		globalAverageAgeWomen /= globalNumberOfWomen;
	}
	
	return true;
}
//=====================================================================================================

bool ShowResults(){
	cout<<"============Show result============\r\n";
	cout<<"\tThe number of attendees is: "<<globalAssistants<<"\r\n";
	cout<<"\tThe age of the youngest assistant: "<<globalAgeOfYoungestPerson<<"\r\n";
	cout<<"\tThe number of men is: "<<globalNumberOfMen<<"\r\n";
	cout<<"\tThe number of women is: "<<globalNumberOfWomen<<"\r\n";
	cout<<"\tThe average age of men is: "<<globalAverageAgeMen<<"\r\n";
	cout<<"\tThe average age of women is: "<<globalAverageAgeWomen<<"\r\n";
	
	return true;	
}
