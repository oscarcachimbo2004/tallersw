/**
 * @file Exercise 1.cpp
 * @author Oscar Lopez(oscar.lopez10@unmsm.edu.pe)
 * @brief Leer un n�mero entero N  y calcule e imprima su factorial N! 
 * @version 1.0
 * @date 02.04.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <stdlib.h>
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int globalN = 0;

int globalFactorial = 1;
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
bool CollectData();
bool Calculate();
bool ShowResults();

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	if(Run() == false){
		cout<<"Error running the program\r\n";
	}
	system("pause");
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

bool Run(){
	while(true){
		if(CollectData() == false){
			cout<<"\r\nError requesting data, please try again\r\n";
			continue;
		}
		if(Calculate() == false){
			cout<<"\r\nError calculating, please try again\r\n";
			continue;
		}
		if(ShowResults() == false){
			cout<<"\r\nError displaying data, try again\r\n";
			continue;
		}
		break;
	}
	
	return true;
}
//=====================================================================================================

bool CollectData(){
	cout<<"============Insert data============\r\n";
	cout<<"Calculation in a course\r\n";
	cout<<"\tType the number to calculate its factorial: ";
	cin>>globalN;
	//Verificamos el dato
	if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n');
		globalN = 0;
		cout<<"Number isn't valid.\r\n";
		return false;
	}
	//Verificamos si no es negativo 
	if(globalN < 0){
		cout<<"The number must exist\r\n";
		return false;
	}
	
	return true;
}
//=====================================================================================================

bool Calculate(){
	
	for(int i=1;i<=globalN;i++){
		 globalFactorial *=i; 
	}
	
	return true;
}
//=====================================================================================================

bool ShowResults(){
	cout<<"============Show result============\r\n";
	cout<<"\r\n";
	
	for(int i = 1;i<=globalN;i++){
		
		if(i == globalN){
			cout<<i<<"!";
		}
		else{
			cout<<i<<"!"<<" X ";
		}
	}
	
	cout<<"\r\n \r\nThe factorial of the entered number is: "<<globalFactorial<<"\r\n";
	
	return true;
}
