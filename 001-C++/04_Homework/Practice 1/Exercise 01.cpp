/**
 * @file Exercise 01.cpp
 * @author Oscar Lopez (oscar.lopez10@unmsm.edu.pe)
 * @brief Elabore un algoritmo que dados como datos  de entrada el radio y 
 		  la altura de un cilindro calcular, el �rea lateral y el volumen del cilindro.
      			 A = 2*PI*radio*altura				V = PI*radio^2*altura
 * @version 1.0
 * @date 22.11.2022
 * 
 */

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include<iostream> //Entrada y salida de datos
#include<math.h> //Permite utilizar funciones matematicas
#include<conio.h> // Para que utilice la funcion getch(), que esta se encarga de ver el ejecutable 

using namespace std;

/*******************************************************************************************************************************************
 *  												DEFINE 
 *******************************************************************************************************************************************/
#define PI 3.141592 //Definimos el valor de pi con define
 
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES 
 *******************************************************************************************************************************************/
float globalInputRadius = 0.0;  //Declaramos los datos de entrada
float globalInputHeight = 0.0;

float globalCylinderArea = 0.0; //Declaramos los datos de salida
float globalCylinderVolume = 0.0;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run(); //Funcion en donde corre el programa
bool CollectData();//Funcion en donde le pedimos datos al usuario  
bool Calculate();//Funcion en donde se van a hallar la altura y el volumen
bool ShowResults();//Funcion en donde se muestra los datos solicitados
float CalculationArea(float radius, float height);//Funcion especializada para hallar el area
float CalculationVolume(float radius, float height);//Funcion especializada para hallar el volumen

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	if(Run() == false){
		cout<<"Program error, check it please\r\n";
	}
	
	getch();
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

bool Run(){
	while(true){
		if(CollectData() == false){
			cout<<"\r\n\tError in collect data, checking and try again\r\n";
			continue;
		}
		if(Calculate() == false){
			cout<<"\r\n\tError in collect data, checking and try again\r\n";
			continue;
		}
		if(ShowResults() == false){
			cout<<"\r\n\tError in collect data, checking and try again\r\n";
			continue;
		}
		break;
	}
	
	return true;
}
//=====================================================================================================

bool CollectData(){
	cout<<"\r\n============Insert data============\r\n";
	cout<<"Calculation in a course\r\n";
	cout<<"\tEnter the cylinder radio to calculate: ";	
    cin>>globalInputRadius;
    //Corroboramos si el numero ingresado,esta en los parametros establecidos
    if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n');
		globalInputHeight = 0.0;
		cout<<"\r\n\t\tIsn't valid!!!\r\n";
		return false;
	}
    cout<<"\tEnter the cylinder Height to calculate: ";	
    cin>>globalInputHeight;
    if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n');
		globalInputHeight = 0.0;
		cout<<"\r\n\t\tIsn't valid!!!\r\n";
		return false;
	}
	
	if(globalInputRadius<=0 && globalInputHeight<=0){
		cout<<"\r\n\tThe numbers entered are not valid.\r\n";
		return false;
	}
	return true;
}
//=====================================================================================================

bool Calculate(){
	
	globalCylinderArea = CalculationArea(globalInputRadius, globalInputHeight); // Valor del area
	globalCylinderVolume = CalculationVolume(globalInputRadius, globalInputHeight); //Valor del volumen
	
	return true;	
}
//=====================================================================================================

bool ShowResults(){
	cout<<"============Show result============\r\n";
	cout<<"\tThe area of the cylinder is: "<<globalCylinderArea<<" square units\r\n"; //Muestra los valores
	cout<<"\tThe volume of the cylinder is: "<<globalCylinderVolume<<" cubic units\r\n";
	
	return true;
}
//=====================================================================================================

float CalculationArea(float radius, float height){
	float area = 0.0;
	
	area = (float)2.0*PI*radius*height; //Utilizamos la formula del problema para hallar el area
	
	return area;
}
//=====================================================================================================

float CalculationVolume(float radius, float height){
	float volume = 0.0;
	
	volume = (float)pow(radius,2)*height*PI; //Utilizamos la formula del problema para hallar el volumen
	
	return volume;
}
