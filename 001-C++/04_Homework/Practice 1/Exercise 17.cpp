/**
 * @file Exercise 17.cpp
 * @author Oscar Lopez (oscar.lopez10@unmsm.edu.pe)
 * @brief El �rea de una elipse se obtiene con la f�rmula PI*ab , donde a es el radio menor de la elipse y b es el radio mayor, 
 		  y su per�metro se obtiene con la f�rmula [2�PI�Sqrt((r1� + r2�)/2) ] . Realice un programa en C ++ utilizando estas 
		   f�rmulas y calcule el �rea y el per�metro de una elipse que tiene un radio menor de 2.5 cm y un radio mayor de 6.4 cm.
 * @version 1.0
 * @date 06.12.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>
#include <conio.h>
using namespace std;

/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/
#define PI 3.141592

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float globalInputSmallerRadius = 2.5;
float globalInputLargerRadius = 6.4;

float globalArea = 0.0;
float globalPerimeter = 0.0;
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
bool Calculate();
bool ShowResults();
float CalculateArea(float smallerRadius, float largerRadius);
float CalculatePerimeter(float smallerRadius, float largerRadius);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	if(Run() == false){
		cout<<"Error running the program\r\n"; //Por si el programa ejecuta mal 
	}
	
	getch();
	  
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

bool Run(){
	while(true){
		if(Calculate() == false){
			cout<<"\r\nError calculating, please try again\r\n";
			continue;
		}
		if(ShowResults() == false){
			cout<<"\r\nError displaying data, try again\r\n";
			continue;
		}
		break;
	}
	
	return true;
}
//=====================================================================================================

bool Calculate(){
	
	globalArea = CalculateArea(globalInputSmallerRadius, globalInputLargerRadius);
	globalPerimeter = CalculatePerimeter(globalInputSmallerRadius, globalInputLargerRadius);
	
	return true;
}
//=====================================================================================================

bool ShowResults(){
	cout<<"============Show result============\r\n";
	cout<<"\tThe area of the ellipse is "<<globalArea<<" square centimeters \r\n";
	cout<<"\tThe perimeter of the ellipse is "<<globalPerimeter<<" centimeters \r\n";
	
	return true;
}
//=====================================================================================================

float CalculateArea(float smallerRadius, float largerRadius){
	float area = 0.0;
	
	area = (float)PI*smallerRadius*largerRadius;
	
	return area;
}
//=====================================================================================================

float CalculatePerimeter(float smallerRadius, float largerRadius){
	float perimeter = 0.0;
	
	perimeter = (float)2.0*PI*(sqrt(pow(smallerRadius,2.0)+pow(largerRadius,2.0))/2.0);
	
	return perimeter;
}
