/**
 * @file Exercise 16.cpp
 * @author Oscar Lopez (oscar.lopez10@unmsm.edu.pe)
 * @brief Escribe, compila y ejecuta un programa en C ++ que calcule y devuelva la ra�z cuarta de un n�mero. 
 		 Pru�balo con el n�mero 81.0 (deber� devolverte 3). Utiliza el programa para calcular la ra�z cuarta de 1728.8964
 * @version 1.0
 * @date 06.12.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>
#include <conio.h>
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
double globalInputNumber = 1728.8964;

double globalFourthRoot = 0.0;
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
bool CollectData();
bool Calculate();
bool ShowResults();
double CalculateFourthRoot(double number);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	if(Run() == false){
		cout<<"Error running the program\r\n"; //Por si el programa ejecuta mal 
	}
	getch();
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

bool Run(){
	while(true){
		if(Calculate() == false){
			cout<<"\r\nError calculating, please try again\r\n";
			continue;
		}
		if(ShowResults() == false){
			cout<<"\r\nError displaying data, try again\r\n";
			continue;
		}
		break;
	}
	
	return true;
}
//=====================================================================================================

bool Calculate(){
	
	globalFourthRoot = CalculateFourthRoot(globalInputNumber);
	
	return true;
}
//=====================================================================================================

bool ShowResults(){
	cout<<"============Show result============\r\n";
	cout<<"\t_The fourth root of "<<globalInputNumber<<" is "<< globalFourthRoot<<" \r\n";
	
	return true;
}
//=====================================================================================================

double CalculateFourthRoot(double number){
	double fourthRoot = 0.0;
	
	fourthRoot = sqrt(sqrt(number));

	return fourthRoot;
}
