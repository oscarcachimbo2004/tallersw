/**
 * @file Exercise 09.cpp
 * @author Oscar Lopez (oscar.lopez10@unmsm.edu.pe)
 * @brief Calcular el monto final, dados como datos el Capital Inicial, el  tipo de Inter�s, 
 		  el numero de periodos por a�o, y el numero de a�os de  la inversi�n. 
		  El c�lculo del capital final se basa en la formula del inter�s compuesto.
				M = C(1+i/N)^T
	Donde:
	M = Capital final o Monto,	C = Capital Inicial,	i = Tipo de inter�s nominal
	N = Numero de periodos por a�o,		T = Numero de 
	
	�Cu�l es el capital que debe colocarse a inter�s  compuesto del 8%  anual para que despu�s de 20 a�os 
	produzca un monto de $ 500,000. ?   
	
 * @version 1.0
 * @date 29.11.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>
#include <conio.h>
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
double globalInputAmount = 500000.0; //Datos de entrada
double globalInputInterest = 0.08;
double globalInputPeriodPerYear = 1.0;
double globalInputNumberOfYears = 20.0;


double globalCapital = 0.0;  //Montos de cada caso que recibiria 
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
bool CollectData();
bool Calculate();
bool ShowResults();
double CalculateCapital(double capital,double interest, double periodPerYear, double numberOfYears);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	if(Run() == false){
		cout<<"Error running the program\r\n"; //Por si el programa ejecuta mal 
	}
	
	getch();
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

bool Run(){
	while(true){
		if(Calculate() == false){
			cout<<"\r\nError calculating, please try again\r\n";
			continue;
		}
		if(ShowResults() == false){
			cout<<"\r\nError displaying data, try again\r\n";
			continue;
		}
		break;
	}
	
	return true;
}
//=====================================================================================================

bool Calculate(){
	
	globalCapital = CalculateCapital(globalInputAmount,globalInputInterest, globalInputPeriodPerYear, globalInputNumberOfYears);
	
	return true;
}
//=====================================================================================================

bool ShowResults(){
	cout<<"============Show result============\r\n";
	cout<<"\tThe principal to be placed at compound interest at 8% per annum so that after 20 years \r\n\t\tit will yield an amount of $500,000 is "<<globalCapital<<" $\r\n";
	
	
	return true;
}
//=====================================================================================================

double CalculateCapital(double amount,double interest, double periodPerYear, double numberOfYears){
	double capital = 0.0;
	
	capital = (double)(amount/pow((1 + (interest/periodPerYear)),numberOfYears));
	
	return capital;
}

