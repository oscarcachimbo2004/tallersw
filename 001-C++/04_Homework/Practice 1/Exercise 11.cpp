/**
 * @file Template.cpp
 * @author Oscar Lopez (oscar.lopez10@unmsm.edu.pe)
 * @brief Hacer un algoritmo para ingresar una medida en metros, y que imprima esa medida expresada en centímetros, 
 		  pulgadas, pies y yardas. Los factores de conversión son los siguientes:
				1 yarda = 3 pies
				1 pie = 12 pulgadas
				1 metro = 100 centímetros
				1 pulgada = 2.54 centímetros

 * @version 1.0
 * @date 30.11.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
double globalInputMeters = 0.0;
 
double globalCentimeters = 0.0;
double globalInches = 0.0;
double globalFeet = 0.0;
double globalYards = 0.0;
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
bool CollectData();
bool Calculate();
bool ShowResults();
double CalculateCentimeters(double meters);
double CalculateInches(double centimeters);
double CalculateFeet(double inches);
double CalculateYards(double feet);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	if(Run() == false){
		cout<<"Error running the program\r\n"; //Por si el programa ejecuta mal 
	}
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

bool Run(){
	while(true){
		if(CollectData() == false){ //Se presenta el centro de control del programa que verifica si esta bien
			cout<<"\r\nError requesting data, please try again\r\n";
			continue;
		}
		if(Calculate() == false){
			cout<<"\r\nError calculating, please try again\r\n";
			continue;
		}
		if(ShowResults() == false){
			cout<<"\r\nError displaying data, try again\r\n";
			continue;
		}
		break;
	}
	
	return true;
}
//=====================================================================================================

bool CollectData(){
	cout<<"============Insert data============\r\n"; //Solicitamos al usuario lo que nos pide
	cout<<"Calculation in a course\r\n";
	cout<<"\tEnter the conversion measurement in meters: ";
	cin>>globalInputMeters;
	if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n'); //Verificamos el dato
		globalInputMeters = 0.0;
		cout<<"Isn't valid.\r\n";
		return false;
	}
	if(globalInputMeters<=0){
		
		return false;
	}
	
	return true;
}
//=====================================================================================================

bool Calculate(){
	
	globalCentimeters = CalculateCentimeters(globalInputMeters);
	globalInches = CalculateInches(globalCentimeters);
	globalFeet = CalculateFeet(globalInches);
	globalYards = CalculateYards(globalFeet);
	
	return true;
}
//=====================================================================================================

bool ShowResults(){
	cout<<"============Show result============\r\n";
	cout<<"\tThe conversion of the measurement entered is: \r\n";
	cout<<"\t\t_ "<<globalCentimeters<<" cm.\r\n";
	cout<<"\t\t_ "<<globalInches<<" inches\r\n";
	cout<<"\t\t_ "<<globalFeet<<" ft.\r\n";
	cout<<"\t\t_ "<<globalYards<<" yards.\r\n";
	
	return true;
}
//=====================================================================================================

double CalculateCentimeters(double meters){
	double centimeters = 0.0;
	
	centimeters = (double)(meters*100.0);
	
	return centimeters;
}
//=====================================================================================================

double CalculateInches(double centimeters){
	double inches = 0.0;
	
	inches = (double)(centimeters)/2.54;
	
	return inches;
}
//=====================================================================================================

double CalculateFeet(double inches){
	double feet = 0.0;
	
	feet = (double)(inches)/12.0;
	
	return feet;
}
//=====================================================================================================

double CalculateYards(double feet){
	double yards = 0.0;
	
	yards = (double)(feet)/3.0;
	
	return yards;
}


