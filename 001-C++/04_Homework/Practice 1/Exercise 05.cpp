/**
 * @file Exercise 05.cpp
 * @author Oscar Lopez (oscar.lopez10@unmsm.edu.pe)
 * @brief El costo de un autom�vil nuevo para un comprador es la suma total del costo del veh�culo, 
 	      del porcentaje de la ganancia del vendedor y de los impuestos locales o estatales 
		  aplicables (sobre el precio de venta). Suponer una ganancia del vendedor del 12% en 
		  todas las unidades y un impuesto del 6% y dise�ar un algoritmo para leer el costo total 
		  del autom�vil e imprimir el costo para el consumidor.
 * @version 1.0
 * @date 27.11.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float globalInputTotalCarPrice = 0.0; 

float globalConsumerCarPrice = 0.0;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
bool CollectData();
bool Calculate();
bool ShowResults();
float CalculateConsumerPrice(float totalCarPrice);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	if(Run() == false){
		cout<<"Error running the program\r\n"; //Por si el programa ejecuta mal 
	}
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

bool Run(){
	while(true){
		if(CollectData() == false){ //Se presenta el centro de control del programa que verifica si esta bien
			cout<<"\r\nError requesting data, please try again\r\n";
			continue;
		}
		if(Calculate() == false){
			cout<<"\r\nError calculating, please try again\r\n";
			continue;
		}
		if(ShowResults() == false){
			cout<<"\r\nError displaying data, try again\r\n";
			continue;
		}
		break;
	}
	
	return true;
}
//=====================================================================================================

bool CollectData(){
	cout<<"============Insert data============\r\n"; //Solicitamos al usuario lo que nos pide
	cout<<"Calculation in a course\r\n";
	cout<<"\tEnter the price of the vehicle: ";
	cin>>globalInputTotalCarPrice;
	if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n'); //Verificamos el dato
		globalInputTotalCarPrice = 0;
		cout<<"Isn't valid.\r\n";
		return false;
	}
	
	return true;
}
//=====================================================================================================

bool Calculate(){
	
	globalConsumerCarPrice = CalculateConsumerPrice(globalInputTotalCarPrice);
	
	return true;
}
//=====================================================================================================

bool ShowResults(){
	cout<<"============Show result============\r\n";
	cout<<"\tThe consumer price of the vehicle is "<<globalConsumerCarPrice<<" $\r\n"; 
	
	
	return true;
}
//=====================================================================================================

float CalculateConsumerPrice(float totalCarPrice){
	float ConsumerPrice = 0.0;
	
	ConsumerPrice = (float)((0.12)*totalCarPrice + (0.06)*totalCarPrice + totalCarPrice);
	
	return ConsumerPrice;
}
