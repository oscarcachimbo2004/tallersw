/**
 * @file Template.cpp
 * @author Oscar Lopez (oscar.lopez10@unmsm.edu.pe)
 * @brief Escriba un programa en C para calcular el valor de la pendiente de una l�nea que conecta dos puntos (x1,y1) y (x2,y2). 
 		  La pendiente est� dada por la ecuaci�n (y2-y1)/(x2-x1). Haga que el programa tambi�n calcule el punto medio de la l�nea 
		  que une los dos puntos, el cual viene dado por (x1+x2)/2,(y1+y2)/2. �Cu�l es el resultado que devuelve el programa para los 
		  puntos (3,7) y (8,12)? 
		   
 * @date 06.12.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <conio.h>
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float globalInputAbscissaX1 = 3.0;
float globalInputOrdinateY1 = 7.0;
float globalInputAbscissaX2 = 8.0;
float globalInputOrdinateY2 = 12.0;

float globalSlope = 0.0; 
float globalMidpointX = 0.0;
float globalMidpointY = 0.0;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
bool CollectData();
bool Calculate();
bool ShowResults();
float CalculateSlope(float x1,float y1,float x2, float y2);
float CalculateMidpoint(float value1,float value2);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	if(Run() == false){
		cout<<"Error running the program\r\n"; //Por si el programa ejecuta mal 
	}
	
	getch();
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

bool Run(){
	while(true){
		if(Calculate() == false){
			cout<<"\r\nError calculating, please try again\r\n";
			continue;
		}
		if(ShowResults() == false){
			cout<<"\r\nError displaying data, try again\r\n";
			continue;
		}
		break;
	}
	
	return true;
}
//=====================================================================================================

bool Calculate(){
	
	globalSlope = CalculateSlope(globalInputAbscissaX1,globalInputOrdinateY1,globalInputAbscissaX2,globalInputOrdinateY2);
	globalMidpointX = CalculateMidpoint(globalInputAbscissaX1,globalInputAbscissaX2);
	globalMidpointY = CalculateMidpoint(globalInputOrdinateY1,globalInputOrdinateY2);
	
	return true;
}
//=====================================================================================================

bool ShowResults(){
	cout<<"============Show result============\r\n";
	cout<<"\t_The slope of the line formed by the two points is "<<globalSlope<<" \r\n";
	cout<<"\t_The midpoint is ("<<globalMidpointX<<" , "<<globalMidpointY<<") \r\n";
	
	return true;
}
//=====================================================================================================

float CalculateSlope(float x1,float y1,float x2, float y2){
	float slope = 0.0;
	
	slope = (float)(y2-y1)/(x2-x1);
	
	return slope;
}
//=====================================================================================================

float CalculateMidpoint(float value1,float value2){
	float midpoint = 0.0;
	
	midpoint = (float)(value1 + value2)/2.0;
	
	return midpoint;
}
