/**
 * @file Exercise 22.cpp
 * @author Oscar Lopez (oscar.lopez10@unmsm.edu.pe)
 * @brief Dado un tiempo expresado en HH:MM y otro tiempo en MM: SS, dise�e un programa que calcule la suma de los tiempos y lo exprese en HH:MM:SS.
 * @version 1.0
 * @date 10.12.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int globalInputHours = 0;
int globalInputFirstMinutes = 0;
int globalInputSecondMinutes = 0;
int globalInputSeconds = 0;

int globalHour = 0;
int globalMinutes = 0;
int globalSeconds = 0;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
bool CollectData();
bool Calculate();
bool ShowResults();
float CalculateTotalMinutes(float firstMinutes,float secondMinutes);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	if(Run() == false){
		cout<<"Error running the program\r\n"; //Por si el programa ejecuta mal 
	}
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

bool Run(){
	while(true){
		if(CollectData() == false){ //Se presenta el centro de control del programa que verifica si esta bien
			cout<<"\r\nError requesting data, please try again\r\n";
			continue;
		}
		if(Calculate() == false){
			cout<<"\r\nError calculating, please try again\r\n";
			continue;
		}
		if(ShowResults() == false){
			cout<<"\r\nError displaying data, try again\r\n";
			continue;
		}
		break;
	}
	
	return true;
}
//=====================================================================================================

bool CollectData(){
	cout<<"============Insert data============\r\n"; //Solicitamos al usuario lo que nos pide
	cout<<"Calculation in a course\r\n";
	cout<<"\tIn the time of HH:MM...\r\n";
	cout<<"\t\tEnter the hours: ";
	cin>>globalInputHours;
	//Corroboramos si el numero ingresado,esta en los parametros establecidos
    if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n');
		globalInputHours = 0.0;
		cout<<"\r\n\t\tIsn't valid!!!\r\n";
		return false;
	}
	cout<<"\t\tEnter the minutes: ";
	cin>>globalInputFirstMinutes;
	//Corroboramos si el numero ingresado,esta en los parametros establecidos
    if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n');
		globalInputFirstMinutes = 0.0;
		cout<<"\r\n\t\tIsn't valid!!!\r\n";
		return false;
	}
	cout<<"\tIn the time of MM:SS...\r\n";
	cout<<"\t\tEnter the minutes: ";
	cin>>globalInputSecondMinutes;
	//Corroboramos si el numero ingresado,esta en los parametros establecidos
    if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n');
		globalInputSecondMinutes = 0.0;
		cout<<"\r\n\t\tIsn't valid!!!\r\n";
		return false;
	}
	cout<<"\t\tEnter the seconds: ";
	cin>>globalInputSeconds;
	//Corroboramos si el numero ingresado,esta en los parametros establecidos
    if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n');
		globalInputSeconds = 0.0;
		cout<<"\r\n\t\tIsn't valid!!!\r\n";
		return false;
	}
	
	if(globalInputHours < 0 || globalInputFirstMinutes < 0 || globalInputSecondMinutes < 0 || globalInputSeconds < 0){
		
		cout<<"\r\n\t\tIsn't valid!!!\r\n";
		
		return false;
	}
	
	if(globalInputHours >= 24 || globalInputFirstMinutes > 60 || globalInputSecondMinutes > 60 || globalInputSeconds > 60){
		
		cout<<"\r\n\t\tIsn't valid!!!\r\n";
		
		return false;
	}
	
	return true;
}
//=====================================================================================================

bool Calculate(){
	
	globalMinutes = CalculateTotalMinutes(globalInputFirstMinutes,globalInputSecondMinutes);
	globalSeconds = globalInputSeconds;
	globalHour = globalInputHours;
	
	if(globalMinutes >= 60){
		
		globalHour += 1;
		globalMinutes = globalMinutes - 60;
		
		if(globalHour >= 24){
			
			globalHour = globalHour - 24;
		}
	}
	
	return true;
}
//=====================================================================================================

bool ShowResults(){
	cout<<"============Show result============\r\n";
	cout<<"\tThe sum of HH:MM and MM:SS is...\r\n";
	cout<<"\t\t"<<globalHour<<" : "<<globalMinutes<<" : "<<globalSeconds<<"\r\n";
	return true;
}
//=====================================================================================================

float CalculateTotalMinutes(float firstMinutes,float secondMinutes){
	int totalMinutes = 0;

	totalMinutes = (int)(firstMinutes + secondMinutes);
	
	return totalMinutes;
}
