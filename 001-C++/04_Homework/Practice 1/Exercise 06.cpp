/**
 * @file Exercise 09.cpp
 * @author Oscar Lopez (oscar.lopez10@unmsm.edu.pe)
 * @brief Desglosar cierta cantidad de segundos a su equivalente en d�as, horas, minutos y segundos.
 * @version 1.0
 * @date 29.11.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <conio.h>
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int globalInputTimeInSeconds = 0;

int globalDays = 0;
int globalHours = 0;
int globalMinutes = 0;
int globalSeconds = 0;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
bool CollectData();
bool Calculate();
bool ShowResults();
int CalculateDays();
int CalculateHours();
int CalculateMinutes();


/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	if(Run() == false){
		cout<<"Error running the program\r\n"; //Por si el programa ejecuta mal 
	}
	
	getch();
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

bool Run(){
	while(true){
		if(CollectData() == false){ //Se presenta el centro de control del programa que verifica si esta bien
			cout<<"\r\nError requesting data, please try again\r\n";
			continue;
		}
		if(Calculate() == false){
			cout<<"\r\nError calculating, please try again\r\n";
			continue;
		}
		if(ShowResults() == false){
			cout<<"\r\nError displaying data, try again\r\n";
			continue;
		}
		break;
	}
	
	return true;
}
//=====================================================================================================

bool CollectData(){
	cout<<"============Insert data============\r\n"; //Solicitamos al usuario lo que nos pide
	cout<<"Calculation in a course\r\n";
	cout<<"Enter the total number of seconds to be broken down: ";
	cin>>globalInputTimeInSeconds;
	if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n'); //Verificamos el dato
		globalInputTimeInSeconds = 0.0;
		cout<<"Isn't valid.\r\n";
		return false;
	}
	
	return true;
}
//=====================================================================================================

bool Calculate(){
	
	globalDays = CalculateDays();
	globalHours = CalculateHours();
	globalMinutes = CalculateMinutes();
	globalSeconds = globalInputTimeInSeconds;
	
	return true;
}
//=====================================================================================================

bool ShowResults(){
	cout<<"============Show result============\r\n";
	cout<<"\tThe conversion is as follows...\r\n";
	cout<<"\t"<<globalDays<<" days\r\n";
	cout<<"\t"<<globalHours<<" hours\r\n";
	cout<<"\t"<<globalMinutes<<" minutes\r\n";
	cout<<"\t"<<globalSeconds<<" seconds\r\n";
	
	return true;
}
//=====================================================================================================

int CalculateDays(){
	int days = 0; 
	
	days = globalInputTimeInSeconds/86400;
	globalInputTimeInSeconds %= 86400;//1 dia = 86400s
	
	return days;
}
//=====================================================================================================

int CalculateHours(){
	int hours = 0;
	
	hours = globalInputTimeInSeconds/3600;
	globalInputTimeInSeconds %= 3600;//1 hora = 3600s
	
	return hours;
}
//=====================================================================================================

int CalculateMinutes(){
	int minutes = 0;
	
	minutes = globalInputTimeInSeconds/60;
	globalInputTimeInSeconds %= 60;//1min = 60s
	
	return minutes;
}
