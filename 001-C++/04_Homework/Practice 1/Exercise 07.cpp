/**
 * @file Template.cpp
 * @author Oscar Lopez (oscar.lopez10@unmsm.edu.pe)
 * @brief Calcular la fuerza de atracci�n entre dos masas, separadas por una distancia, 
 		  mediante la siguiente f�rmula:
				F = G*masa1*masa2 / distancia^2
			Donde G es la constante de gravitaci�n universal: G = 6.673 * 10^-8 cm3/g.seg2

 * @version 1.0
 * @date 29.11.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>
#include <conio.h>
using namespace std;
/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/
 #define G 6.673*pow(10,-8)

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
double globalInputMass1 = 0.0;
double globalInputMass2 = 0.0;
double globalInputDistance = 0.0;

double globalGravitationalForce = 0.0;
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
bool CollectData();
bool Calculate();
bool ShowResults();
double CalculateGravitationalForce(double mass1,double mass2,double distance);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	if(Run() == false){
		cout<<"Error running the program\r\n"; //Por si el programa ejecuta mal 
	}
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

bool Run(){
	while(true){
		if(CollectData() == false){ //Se presenta el centro de control del programa que verifica si esta bien
			cout<<"\r\nError requesting data, please try again\r\n";
			continue;
		}
		if(Calculate() == false){
			cout<<"\r\nError calculating, please try again\r\n";
			continue;
		}
		if(ShowResults() == false){
			cout<<"\r\nError displaying data, try again\r\n";
			continue;
		}
		break;
	}
	
	return true;
}
//=====================================================================================================

bool CollectData(){
	cout<<"============Insert data============\r\n"; //Solicitamos al usuario lo que nos pide
	cout<<"Calculation in a course\r\n";
	cout<<"\tEnter mass 1: ";
	cin>>globalInputMass1;
	if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n'); //Verificamos el dato
		globalInputMass1 = 0.0;
		cout<<"Isn't valid.\r\n";
		return false;
	}
	cout<<"\tEnter mass 2: ";
	cin>>globalInputMass2;
	if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n'); //Verificamos el dato
		globalInputMass2 = 0.0;
		cout<<"Isn't valid.\r\n";
		return false;
	}
	cout<<"\tEnter distance: ";
	cin>>globalInputDistance;
	if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n'); //Verificamos el dato
		globalInputDistance = 0.0;
		cout<<"Isn't valid.\r\n";
		return false;
	}
	
	return true;
}
//=====================================================================================================

bool Calculate(){
	
	globalGravitationalForce = CalculateGravitationalForce(globalInputMass1,globalInputMass2,globalInputDistance);
	
	return true;
}
//=====================================================================================================

bool ShowResults(){
	cout<<"============Show result============\r\n";
	cout<<"\tThe gravitational force existing between the two masses is "<<globalGravitationalForce<<" N\r\n";
	return true;
}
//=====================================================================================================

double CalculateGravitationalForce(double mass1,double mass2,double distance){
	double gravitationalForce = 0.0;
	
	gravitationalForce = (double)(mass1*mass2*G)/pow(distance,2);
	
	return gravitationalForce;
}
