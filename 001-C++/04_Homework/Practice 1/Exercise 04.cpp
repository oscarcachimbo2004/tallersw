/**
 * @file Exercise 04.cpp
 * @author Oscar Lopez (oscar.lopez10@unmsm.edu.pe)
 * @brief Un departamento de climatolog�a ha realizado recientemente su conversi�n al sistema m�trico. 
 		  Dise�ar un algoritmo para realizar las siguientes conversiones:
			a. Leer la temperatura dada en la escala Celsius e imprimir en su 
			equivalente Fahrenheit (la f�rmula de conversi�n es "F=9/5 �C+32").
			b. Leer la cantidad de agua en pulgadas e imprimir su equivalente en mil�metros (25.5 mm = 1pulgada)
 * @version 1.0
 * @date 27.11.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <conio.h>
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float globalInputCelciusTemperature = 0;
float globalInputInchesWater = 0;

float globalFahrenheitTemperature = 0;
float globalMillimetersWater = 0;
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
bool CollectData();
bool Calculate();
bool ShowResults();
float ConversionToFahrenheit(int celsius);
float ConversionToInches(int millimetersWater);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	if(Run() == false){
		cout<<"Error running the program\r\n"; //Por si el programa ejecuta mal 
	}
	
	getch();
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

bool Run(){
	while(true){
		if(CollectData() == false){ //Se presenta el centro de control del programa que verifica si esta bien
			cout<<"\r\nError requesting data, please try again\r\n";
			continue;
		}
		if(Calculate() == false){
			cout<<"\r\nError calculating, please try again\r\n";
			continue;
		}
		if(ShowResults() == false){
			cout<<"\r\nError displaying data, try again\r\n";
			continue;
		}
		break;
	}
	
	return true;
}
//=====================================================================================================

bool CollectData(){
	cout<<"============Insert data============\r\n"; //Solicitamos al usuario lo que nos pide
	cout<<"Calculation in a course\r\n";
	cout<<"\tEnter the temperature in degrees Fahrenheit: ";
	cin>>globalInputCelciusTemperature;
	if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n'); //Verificamos el dato
		globalInputCelciusTemperature = 0.0;
		cout<<"Isn't valid.\r\n";
		return false;
	}
	cout<<"\t Enter the size of the water in inches: ";
	cin>>globalInputInchesWater;
	 if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n');
		globalInputInchesWater = 0.0;
		cout<<"Isn't valid.\r\n";
		return false;
	}
	
	return true;
}
//=====================================================================================================

bool Calculate(){
	globalFahrenheitTemperature = ConversionToFahrenheit(globalInputCelciusTemperature);
	globalMillimetersWater = ConversionToInches(globalInputInchesWater);
	
	return true;
}
//=====================================================================================================

bool ShowResults(){
	cout<<"============Show result============\r\n";
	cout<<"\tThe conversion of temperature to kelvin is: "<<globalFahrenheitTemperature<<" K\r\n";
	cout<<"\tThe measurement of water in inches is: "<<globalMillimetersWater<<" inches\r\n";
	
	return true;
}
//=====================================================================================================

float ConversionToFahrenheit(int celsius){
	float fahrenheit = 0.0;
	
	fahrenheit = (float)((9.0*celsius)/5.0) + 32.0;
	
	return fahrenheit;
}
//=====================================================================================================

float ConversionToInches(int millimetersWater){
	float inches = 0.0;
	
	inches = (float)millimetersWater*25.5;
	
	return inches;
}

