/**
 * @file Exercise 23.cpp
 * @author Oscar Lopez (oscar.lopez10@unmsm.edu.pe)
 * @brief En todo triangulo se cumple que cada lado es proporcional al seno del ángulo opuesto. Esta ley se llama la ley de los senos Matemáticamente.
							
									a/sen(alfa) = b/sen(beta) =c/sen(gamma)
									 
	Si se conocen los ángulos  alfa , beta , gamma  y el lado c. ¿Cuanto valen los otros dos lados? 
 * @version 1.0
 * @date 11.12.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>
#include <conio.h>
using namespace std;

/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/
#define PI 3.14159265358979

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float globalInputAlphaAngle = 0.0;
float globalInputBetaAngle = 0.0;
float globalInputGammaAngle = 0.0;
float globalInputSideC = 0.0;

float globalSideA = 0.0;
float globalSideB = 0.0;
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
bool CollectData();
bool Calculate();
bool ShowResults();
float CalculateSide(float side, float angle1, float angle2);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	if(Run() == false){
		cout<<"Error running the program\r\n"; //Por si el programa ejecuta mal 
	}
	
	getch();
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

bool Run(){
	while(true){
		if(CollectData() == false){ //Se presenta el centro de control del programa que verifica si esta bien
			cout<<"\r\nError requesting data, please try again\r\n";
			continue;
		}
		if(Calculate() == false){
			cout<<"\r\nError calculating, please try again\r\n";
			continue;
		}
		if(ShowResults() == false){
			cout<<"\r\nError displaying data, try again\r\n";
			continue;
		}
		break;
	}
	
	return true;
}
//=====================================================================================================

bool CollectData(){
	cout<<"============Insert data============\r\n"; //Solicitamos al usuario lo que nos pide
	cout<<"Calculation in a course\r\n";
	cout<<"\tEnter the alpha angle: ";
	cin>>globalInputAlphaAngle;
	//Corroboramos si el numero ingresado,esta en los parametros establecidos
    if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n');
		globalInputAlphaAngle = 0.0;
		cout<<"\r\n\t\tIsn't valid!!!\r\n";
		return false;
	}
	cout<<"\tEnter the beta angle: ";
	cin>>globalInputBetaAngle;
	//Corroboramos si el numero ingresado,esta en los parametros establecidos
    if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n');
		globalInputBetaAngle = 0.0;
		cout<<"\r\n\t\tIsn't valid!!!\r\n";
		return false;
	}
	cout<<"\tEnter the gamma angle: ";
	cin>>globalInputGammaAngle;
	//Corroboramos si el numero ingresado,esta en los parametros establecidos
    if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n');
		globalInputGammaAngle = 0.0;
		cout<<"\r\n\t\tIsn't valid!!!\r\n";
		return false;
	}
	cout<<"\tEnter side 'c': ";
	cin>>globalInputSideC;
	//Corroboramos si el numero ingresado,esta en los parametros establecidos
    if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n');
		globalInputSideC = 0.0;
		cout<<"\r\n\t\tIsn't valid!!!\r\n";
		return false;
	}
	if(globalInputAlphaAngle <= 0 || globalInputBetaAngle <= 0 || globalInputGammaAngle <= 0 || globalInputSideC <= 0){
		
		cout<<"\r\n\t\tIsn't valid!!!\r\n";
		
		return false;
	}
	if((globalInputAlphaAngle + globalInputBetaAngle + globalInputGammaAngle) != 180.0){
		
		cout<<"\r\n\t\tIsn't valid!!!\r\n";
		
		return false;
	}
	return true;
}
//=====================================================================================================

bool Calculate(){
	
	globalSideA = CalculateSide(globalInputSideC, globalInputGammaAngle, globalInputAlphaAngle);
	globalSideB = CalculateSide(globalInputSideC, globalInputGammaAngle, globalInputBetaAngle);
	
	return true;
}
//=====================================================================================================

bool ShowResults(){
	cout<<"============Show result============\r\n";
	cout<<"\tSide 'a' is "<<globalSideA<<" unites \r\n";
	cout<<"\tSide 'b' is "<<globalSideB<<" unites \r\n";
	
	return true;
}
//=====================================================================================================

float CalculateSide(float side, float angle1, float angle2){
	float sides = 0.0;
	float angleRadians1 = 0.0;
	float angleRadians2 = 0.0;
	
	angleRadians1 = (float)(angle1*PI)/(180.0);
	angleRadians2 = (float)(angle2*PI)/(180.0);
	sides = (float)(sin(angleRadians2)*side)/sin(angleRadians1);
	
	return sides;
}
