/**
 * @file Exercise 10.cpp
 * @author Oscar Lopez (oscar.lopez10@unmsm.edu.pe)
 * @brief Un millonario exc�ntrico ten�a tres hijos: Carlos, Jos� y Marta. Al morir dej� el siguiente legado: A Jos� le dej� 4/3 de 
 		  lo que le dej� a Carlos. A Carlos le dej� 1/3 de su fortuna. A Marta le dejo la mitad de lo que le dej� a Jos�. Preparar 
		  un algoritmo para darle la suma a repartir e imprima cuanto le toc� a cada uno.
 * @version 1.0
 * @date 29.11.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <conio.h>
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float globalInputTotalCash = 0.0;

float globalJoseMoney = 0.0;
float globalCarlosMoney = 0.0;
float globalMartaMoney = 0.0;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
bool CollectData();
bool Calculate();
bool ShowResults();
float CalculateJoseMoney(float carlosMoney);
float CalculateCarlosMoney();
float CalculateMartaMoney(float joseMoney);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	if(Run() == false){
		cout<<"Error running the program\r\n"; //Por si el programa ejecuta mal 
	}
	
	getch();
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

bool Run(){
	while(true){
		if(CollectData() == false){ //Se presenta el centro de control del programa que verifica si esta bien
			cout<<"\r\nError requesting data, please try again\r\n";
			continue;
		}
		if(Calculate() == false){
			cout<<"\r\nError calculating, please try again\r\n";
			continue;
		}
		if(ShowResults() == false){
			cout<<"\r\nError displaying data, try again\r\n";
			continue;
		}
		break;
	}
	
	return true;
}
//=====================================================================================================

bool CollectData(){
	cout<<"============Insert data============\r\n"; //Solicitamos al usuario lo que nos pide
	cout<<"Calculation in a course\r\n";
	cout<<"\tEnter the total amount to be distributed: ";
	cin>>globalInputTotalCash;
	if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n'); //Verificamos el dato
		globalInputTotalCash = 0.0;
		cout<<"Isn't valid.\r\n";
		return false;
	}
	
	if(globalInputTotalCash <= 0){
		
		return false;
	}
	
	return true;
}
//=====================================================================================================

bool Calculate(){
	
	globalCarlosMoney = CalculateCarlosMoney();
	globalJoseMoney = CalculateJoseMoney(globalCarlosMoney);
	globalMartaMoney = CalculateMartaMoney(globalJoseMoney);
	
	return true;
}
//=====================================================================================================

bool ShowResults(){
	cout<<"============Show result============\r\n";
	cout<<"\tThe distributed inheritance will be as follows....\r\n";
	cout<<"\t\tJose is responsible for "<<globalJoseMoney<<" $\r\n";
	cout<<"\t\tCarlos is responsible for "<<globalCarlosMoney<<" $\r\n";
	cout<<"\t\tMarta is responsible for "<<globalMartaMoney<<" $\r\n";
	
	return true;
}
//=====================================================================================================

float CalculateJoseMoney(float carlosMoney){
	float joseMoney = 0.0;
	
	joseMoney = (float)(4*carlosMoney)/3.0;
	
	globalInputTotalCash = globalInputTotalCash - joseMoney;
	
	return joseMoney;
}
//=====================================================================================================

float CalculateCarlosMoney(){
	float carlosMoney = 0.0;
	
	carlosMoney =(float)(globalInputTotalCash/3.0);
	
	globalInputTotalCash = globalInputTotalCash - carlosMoney;
	
	return carlosMoney;
}
//=====================================================================================================

float CalculateMartaMoney(float joseMoney){
	
	float martaMoney = 0.0;
	
	martaMoney = (float)(joseMoney)/2;
	
	globalInputTotalCash = globalInputTotalCash-martaMoney;
	
	return martaMoney;
}
