/**
 * @file Exercise 08.cpp
 * @author Oscar Lopez (oscar.lopez10@unmsm.edu.pe)
 * @brief Calcular el monto final, dados como datos el Capital Inicial, el  tipo de Inter�s, 
 		  el numero de periodos por a�o, y el numero de a�os de  la inversi�n. 
		  El c�lculo del capital final se basa en la formula del inter�s compuesto.
				M = C(1+i/N)^T
	Donde:
	M = Capital final o Monto,	C = Capital Inicial,	i = Tipo de inter�s nominal
	N = Numero de periodos por a�o,		T = Numero de a�os
	
	- Si un cliente deposita al Banco la cantidad de $10,000 a inter�s compuesto con 
	  una tasa del 8% anual.  �Cual ser� el monto que recaude despu�s de 9 a�os? 
 - �Cuanto debe cobrar el cliente dentro de 3 a�os si deposita  $ 100,000. al 9% anual  
 	y capitaliz�ndose los intereses bimestralmente?   
	
 * @version 1.0
 * @date 29.11.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>
#include <conio.h>
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
double globalInputCapital1 = 10000.0; //Datos de la primera interrogant
double globalInputInterest1 = 0.08;
double globalInputPeriodPerYear1 = 1.0;
double globalInputNumberOfYears1 = 9.0;

double globalInputCapital2 = 100000.0; //Datos de la segunda interrogante
double globalInputInterest2 = 0.09;
double globalInputPeriodPerYear2 = 6;
double globalInputNumberOfYears2 = 3;

double globalAmount1 = 0.0;  //Montos de cada caso que recibiria 
double globalAmount2 = 0.0;
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
bool CollectData();
bool Calculate();
bool ShowResults();
double CalculateAmount(double capital,double interest, double periodPerYear, double numberOfYears);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	if(Run() == false){
		cout<<"Error running the program\r\n"; //Por si el programa ejecuta mal 
	}
	
	getch();
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

bool Run(){
	while(true){
		if(Calculate() == false){
			cout<<"\r\nError calculating, please try again\r\n";
			continue;
		}
		if(ShowResults() == false){
			cout<<"\r\nError displaying data, try again\r\n";
			continue;
		}
		break;
	}
	
	return true;
}
//=====================================================================================================

bool Calculate(){
	
	globalAmount1 = CalculateAmount(globalInputCapital1,globalInputInterest1, globalInputPeriodPerYear1, globalInputNumberOfYears1);
	globalAmount2 = CalculateAmount(globalInputCapital2,globalInputInterest2, globalInputPeriodPerYear2, globalInputNumberOfYears2);
	
	return true;
}
//=====================================================================================================

bool ShowResults(){
	cout<<"============Show result============\r\n";
	cout<<"\tIf a customer deposits $10,000 with the Bank at compound interest at a rate of 8% per annum, \r\n\t\tthe amount received will be "<<globalAmount1<<" $\r\n";
	cout<<"\r\n\tIf a customer deposits $100,000 with the Bank at compound interest at a rate of 9% per annum, \r\n\t\tthe amount received will be "<<globalAmount2<<" $\r\n";
	
	return true;
}
//=====================================================================================================

double CalculateAmount(double capital,double interest, double periodPerYear, double numberOfYears){
	double amount = 0.0;
	
	amount = (double)(capital*pow((1 + (interest/periodPerYear)),numberOfYears));
	
	return amount;
}

