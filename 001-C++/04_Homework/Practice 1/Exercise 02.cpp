/**
 * @file Exercise 02.cpp
 * @author Oscar Lopez (oscar.lopez10@unmsm.edu.pe)
 * @brief Un maestro desea saber que porcentaje de hombres 
 		  y que porcentaje de mujeres hay en un grupo de estudiantes. 
 * @version 1.0
 * @date 23.11.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int globalInputMaleStudents = 0;//Datos de entrada 
int globalInputFemaleStudents = 0;

int globalTotalStudents = 0.0;//Dato intermediario, necesario para hallar el porcentaje
float globalMaleStudentsPercentage = 0.0;//Datos de salida
float globalFemaleStudentsPercentage = 0.0;
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
bool CollectData();
bool Calculate(); 
bool ShowResults();
int TotalStundents(int maleStudents,int femaleStudents);
float StudentsPercentage(int students, int totalStundents);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	if(Run() == false){
		cout<<"Error running the program\r\n"; //Por si el programa ejecuta mal 
	}
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

bool Run(){
	while(true){
		if(CollectData() == false){ //Se presenta el centro de control del programa que verifica si esta bien
			cout<<"\r\nError requesting data, please try again\r\n";
			continue;
		}
		if(Calculate() == false){
			cout<<"\r\nError calculating, please try again\r\n";
			continue;
		}
		if(ShowResults() == false){
			cout<<"\r\nError displaying data, try again\r\n";
			continue;
		}
		break;
	}
	
	return true;
}
//=====================================================================================================

bool CollectData(){
	cout<<"\r\n============Insert data============\r\n"; //Solicitamos al usuario lo que nos pide
	cout<<"Calculation in a course\r\n";
	cout<<"\tEnter the number of male students: ";
	cin>>globalInputMaleStudents;
	if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n'); //Verificamos el dato
		globalInputMaleStudents = 0.0;
		cout<<"Isn't valid.\r\n";
		return false;
	}
	cout<<"\tEnter the number of female students: ";
	cin>>globalInputFemaleStudents;
	if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n');
		globalInputFemaleStudents = 0.0;
		cout<<"Isn't valid.\r\n";
		return false;
	}
	
	if(globalInputFemaleStudents<=0 && globalInputMaleStudents<=0){
		cout<<"\r\n\tThe numbers entered are not valid.\r\n";
		return false;
	}
	return true;
}
//=====================================================================================================

bool Calculate(){
	//Hacemos uso de las funciones para hallar el porcentaje de los estudiantes hombres y mujeres.
	globalTotalStudents = TotalStundents(globalInputMaleStudents,globalInputFemaleStudents);
	globalMaleStudentsPercentage = StudentsPercentage(globalInputMaleStudents, globalTotalStudents);
	globalFemaleStudentsPercentage = StudentsPercentage(globalInputFemaleStudents,globalTotalStudents);
	
	return true;
}
//=====================================================================================================

bool ShowResults(){
	cout<<"\r\n============Show result============\r\n";
	cout<<"\tThe percentage of male students is: "<<globalMaleStudentsPercentage<<" %\r\n"; //Muestra los valores
	cout<<"\tThe percentage of female students is: "<<globalFemaleStudentsPercentage<<" %\r\n";
	
	
	return true;
}
//=====================================================================================================

int TotalStundents(int maleStudents,int femaleStudents){
	int totalStudents = 0;
	
	totalStudents = maleStudents + femaleStudents; //Funcion encargada en hallar el total de estudiantes
	
	return totalStudents;
}
//=====================================================================================================

float StudentsPercentage(int students, int totalStundents){
	float percentajeStudents = 0.0;
	
	percentajeStudents = (float)(students)/(totalStundents)*100.0; //Halla tanto el porcentaje de estudiantes
																 //mujeres como hombres
	return percentajeStudents;
}

