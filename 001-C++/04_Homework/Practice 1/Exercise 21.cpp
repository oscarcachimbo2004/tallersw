/**
 * @file Exercise 21.cpp
 * @author Oscar Lopez (oscar.lopez10@unmsm.edu.pe)
 * @brief Calcular la velocidad de un auto en Km/h, ingresando la distancia recorrida en metros y el tiempo en minutos.
 * @version 1.0
 * @date 10.12.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <conio.h>
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float globalInputDistance = 0.0;
float globalInputTimeInMinutes = 0.0;

float globalTimeInSeconds = 0.0;
float globalSpeed = 0.0; //Va a estar convertida en km
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
bool CollectData();
bool Calculate();
bool ShowResults();
float CalculateTimeInSeconds(float minutes);
float CalculateSpeed(float distance,float time);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	if(Run() == false){
		cout<<"Error running the program\r\n"; //Por si el programa ejecuta mal 
	}
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

bool Run(){
	while(true){
		if(CollectData() == false){ //Se presenta el centro de control del programa que verifica si esta bien
			cout<<"\r\nError requesting data, please try again\r\n";
			continue;
		}
		if(Calculate() == false){
			cout<<"\r\nError calculating, please try again\r\n";
			continue;
		}
		if(ShowResults() == false){
			cout<<"\r\nError displaying data, try again\r\n";
			continue;
		}
		break;
	}
	
	return true;
}
//=====================================================================================================

bool CollectData(){
	cout<<"============Insert data============\r\n"; //Solicitamos al usuario lo que nos pide
	cout<<"Calculation in a course\r\n";
	cout<<"\tEnter the distance you are traveling in meters: ";
	cin>>globalInputDistance;
	//Corroboramos si el numero ingresado,esta en los parametros establecidos
    if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n');
		globalInputDistance = 0.0;
		cout<<"\r\n\t\tIsn't valid!!!\r\n";
		return false;
	}
	cout<<"\tEnter the time it takes in minutes: ";
	cin>>globalInputTimeInMinutes;
	//Corroboramos si el numero ingresado,esta en los parametros establecidos
    if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n');
		globalInputTimeInMinutes = 0.0;
		cout<<"\r\n\t\tIsn't valid!!!\r\n";
		return false;
	}
	
	if(globalInputDistance <=0 || globalInputTimeInMinutes<=0){
		
		cout<<"\r\n\t\tIsn't valid!!!\r\n";
		
		return false;
	}
	
	return true;
}
//=====================================================================================================

bool Calculate(){
	
	globalTimeInSeconds = CalculateTimeInSeconds(globalInputTimeInMinutes);
	globalSpeed = CalculateSpeed(globalInputDistance,globalTimeInSeconds); 
	
	return true;
}
//=====================================================================================================

bool ShowResults(){
	cout<<"============Show result============\r\n";
	cout<<"\tThe speed is "<<globalSpeed<<" km/h \r\n";
	
	return true;
}
//=====================================================================================================

float CalculateTimeInSeconds(float minutes){
	float seconds = 0.0;
	
	seconds = (float)minutes*(60.0);
	
	return seconds;
}
//=====================================================================================================

float CalculateSpeed(float distance,float time){
	float speed = 0.0;
	
	speed = (float)(5.0/18.0)*(distance/time);
	
	return speed;
}
