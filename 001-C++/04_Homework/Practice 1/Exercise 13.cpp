/**
 * @file Exercise 13.cpp
 * @author Oscar Lopez (oscar.lopez10@unmsm.edu.pe)
 * @brief Un tonel es un recipiente, generalmente de madera, muy utilizado para almacenar y mejorar un vino. La forma de un tonel 
 		  es muy caracter�stica y es un cilindro en el que la parte central es m�s gruesa, es decir, tiene un di�metro mayor 
		  que los extremos. Escriba un programa que lea las medidas de un tonel y nos devuelva su
		  capacidad, teniendo en cuenta que el volumen (V) de un tonel viene dado por la siguiente f�rmula: V = pi*l*a^2 donde:
				l    es la longitud del tonel, su altura. 
				d   es el di�metro del tonel en sus extremos.
				D  es el di�metro del tonel en el centro: D>d
				a = d/2 + 2/3(D/2 - d/2)
			Nota: Observe que si las medidas se dan en cent�metros el resultado lo obtenemos en cent�metros c�bicos.
 * @version 1.0
 * @date 30.11.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>
#include <conio.h>
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float globalInputBarrelLength = 0.0;
float globalInputBarrelDiameterAtTheEnds = 0.0;
float globalInputBarrelDiameterAtCenter = 0.0;

float globalVolume = 0.0;
/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/
 #define PI 3.141592
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
bool CollectData();
bool Calculate();
bool ShowResults();
float CalculateVolume(float length,float diameterAtTheEnds,float diameterAtCenter);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	if(Run() == false){
		cout<<"Error running the program\r\n"; //Por si el programa ejecuta mal 
	}
	getch();
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

bool Run(){
	while(true){
		if(CollectData() == false){ //Se presenta el centro de control del programa que verifica si esta bien
			cout<<"\r\nError requesting data, please try again\r\n";
			continue;
		}
		if(Calculate() == false){
			cout<<"\r\nError calculating, please try again\r\n";
			continue;
		}
		if(ShowResults() == false){
			cout<<"\r\nError displaying data, try again\r\n";
			continue;
		}
		break;
	}
	
	return true;
}
//=====================================================================================================

bool CollectData(){
	cout<<"============Insert data============\r\n"; //Solicitamos al usuario lo que nos pide
	cout<<"Calculation in a course\r\n";
	cout<<"\tEnter the length of the barrel in centimeters: ";
	cin>>globalInputBarrelLength;
	if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n'); //Verificamos el dato
		globalInputBarrelLength = 0.0;
		cout<<"Isn't valid.\r\n";
		return false;
	}
	cout<<"\tEnter the diameter of the barrel at its ends in centimeters: ";
	cin>>globalInputBarrelDiameterAtTheEnds;
	if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n'); //Verificamos el dato
		globalInputBarrelDiameterAtTheEnds = 0.0;
		cout<<"Isn't valid.\r\n";
		return false;
	}
	cout<<"\tEnter the diameter of the barrel at the center in centimeters: ";
	cin>>globalInputBarrelDiameterAtCenter;
	if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n'); //Verificamos el dato
		globalInputBarrelDiameterAtCenter = 0.0;
		cout<<"Isn't valid.\r\n";
		return false;
	}
	if(globalInputBarrelLength<=0 && globalInputBarrelDiameterAtTheEnds<=0 && globalInputBarrelDiameterAtCenter<=0){
		
		return false;
	}
	
	if(globalInputBarrelDiameterAtCenter < globalInputBarrelDiameterAtTheEnds){
		cout<<"\r\n\tThe diameter of the center must be larger than the diameter of the ends of the barrel.";
		
		return false;
	}

	return true;
}
//=====================================================================================================

bool Calculate(){
	
	globalVolume = CalculateVolume(globalInputBarrelLength,globalInputBarrelDiameterAtTheEnds,globalInputBarrelDiameterAtCenter);
	
	return true;
}
//=====================================================================================================

bool ShowResults(){
	cout<<"============Show result============\r\n";
	cout<<"\t_The volume of the barrel is "<<globalVolume<<" cubic centimeters\r\n";
	
	return true;
}
//=====================================================================================================

float CalculateVolume(float length,float diameterAtTheEnds,float diameterAtCenter){
	float valueOfa = 0.0;
	float volume = 0.0;
	
	valueOfa = (float)((diameterAtTheEnds/2.0) + 2/3*((diameterAtCenter/2.0)-(diameterAtTheEnds/2.0)));		//a = d/2 + 2/3(D/2 - d/2)
	volume = (float)(PI)*(length)*(pow(valueOfa,2)); //V = pi*l*a^2
	
	return volume;
}
//=====================================================================================================

