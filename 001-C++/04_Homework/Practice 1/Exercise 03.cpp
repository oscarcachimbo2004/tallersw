/**
 * @file Exercise 03.cpp
 * @author Oscar Lopez (oscar.lopez10@unmsm.edu.pe)
 * @brief Queremos conocer los datos estad�sticos de una asignatura, por lo tanto, 
 		  necesitamos un algoritmo que lea el n�mero de desaprobados, aprobados, notables y 
		   sobresalientes de una asignatura, y nos devuelva:
				a. El tanto por ciento de alumnos que han superado la asignatura.
				b. El tanto por ciento de desaprobados, aprobados, notables y sobresalientes de la 
				   asignatura
 * @version 1.0
 * @date 25.11.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <conio.h>
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int globalInputUnsuccessful = 0;
int globalInputApproved = 0;
int globaalInputNotable = 0;
int globalInputOutstanding = 0;

int gloabalTotalStudents = 0;
float globalPassTheCoursePercentage = 0;
float globalUnsuccessfulPercentage = 0.0;
float globalApprovedPercentage = 0.0;
float globalNotablePercentage = 0.0;
float globalOutstandingPercentage = 0.0;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
bool CollectData();
bool Calculate();
bool ShowResults();
int TotalStudents(int unsuccessful,int approved,int notable,int outstanding);
float PassedTheCoursePercentage(int approved,int notable,int outstanding,int totalStudents); //Para la pregunta A
float StudentsPercentage(int totalStudents, int students); // Para la pregunto B 

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	if(Run() == false){
		cout<<"Error running the program\r\n"; //Por si el programa ejecuta mal 
	}
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

bool Run(){
	while(true){
		if(CollectData() == false){ //Se presenta el centro de control del programa que verifica si esta bien
			cout<<"\r\nError requesting data, please try again\r\n";
			continue;
		}
		if(Calculate() == false){
			cout<<"\r\nError calculating, please try again\r\n";
			continue;
		}
		if(ShowResults() == false){
			cout<<"\r\nError displaying data, try again\r\n";
			continue;
		}
		break;
	}
	
	return true;
}
//=====================================================================================================

bool CollectData(){
	cout<<"============Insert data============\r\n"; //Solicitamos al usuario lo que nos pide
	cout<<"Calculation in a course\r\n";
	cout<<"\tEnter the number of failed students: ";
	cin>>globalInputUnsuccessful;
	if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n'); //Verificamos el dato
		globalInputUnsuccessful = 0.0;
		cout<<"Isn't valid.\r\n";
		return false;
	}
	cout<<"\tEnter the number of approved students: ";
	cin>>globalInputApproved;
	if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n'); //Verificamos el dato
		globalInputApproved = 0.0;
		cout<<"Isn't valid.\r\n";
		return false;
	}
	cout<<"\tEnter the number of notable students: ";
	cin>>globaalInputNotable;
	if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n'); //Verificamos el dato
		globaalInputNotable = 0.0;
		cout<<"Isn't valid.\r\n";
		return false;
	}
	cout<<"\tEnter the number of outstanding students: ";
	cin>>globalInputOutstanding;
	if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n'); //Verificamos el dato
		globalInputOutstanding = 0.0;
		cout<<"Isn't valid.\r\n";
		return false;
	}
	
	if(globalInputOutstanding<=0 && globaalInputNotable<=0 && globalInputApproved<=0 && globalInputUnsuccessful<=0){
		cout<<"\r\n\tThe numbers entered are not valid.\r\n";
		return false;
	}
	
	return true;
}
//=====================================================================================================

bool Calculate(){
	
	gloabalTotalStudents = TotalStudents(globalInputUnsuccessful,globalInputApproved,globaalInputNotable,globalInputOutstanding);
	globalPassTheCoursePercentage = PassedTheCoursePercentage(globalInputApproved,globaalInputNotable,globalInputOutstanding,gloabalTotalStudents);
	globalUnsuccessfulPercentage = StudentsPercentage(gloabalTotalStudents,globalInputUnsuccessful);
	globalApprovedPercentage = StudentsPercentage(gloabalTotalStudents,globalInputApproved);
	globalNotablePercentage = StudentsPercentage(gloabalTotalStudents,globaalInputNotable);
	globalOutstandingPercentage = StudentsPercentage(gloabalTotalStudents,globalInputOutstanding);
	
	return true;
}
//=====================================================================================================

bool ShowResults(){
	cout<<"============Show result============\r\n";
	cout<<"\tThe percentage of students who have passed the course is: "<<globalPassTheCoursePercentage<<" %\r\n";
	cout<<"\tThe percentage of students failing is: "<<globalUnsuccessfulPercentage<<" %\r\n"; 
	cout<<"\tThe percentage of students passing is: " <<globalApprovedPercentage<<" %\r\n";
	cout<<"\tThe percentage of notable students is: "<<globalNotablePercentage<<" %\r\n";
	cout<<"\tThe percentage of outstanding students is: "<<globalOutstandingPercentage<<" %\r\n";
	
	return true;
}
//=====================================================================================================

int TotalStudents(int unsuccessful,int approved,int notable,int outstanding){
	int total = 0;
	
	total = unsuccessful + approved + notable + outstanding;
	
	return total;
}
//=====================================================================================================

float PassedTheCoursePercentage(int approved,int notable,int outstanding,int totalStudents){
	float percentage = 0;//Porcentaje de alumnos que pasaron la asignatura
	
	percentage = (float)(approved+notable+outstanding)*100.0/(totalStudents);
	
	return percentage;
}
//=====================================================================================================

float StudentsPercentage(int totalStudents, int students){
	float percentage = 0; //porcentage de alumnos
	
	percentage = (float)(students*100.0)/totalStudents;
	
	return percentage;
}
