/**
 * @file Exercise 18.cpp
 * @author Oscar Lopez (oscar.lopez10@unmsm.edu.pe)
 * @brief Se coloca una escalera de 3 metros a un �ngulo de 85 grados al lado de un edificio, la altura en la cual la 
 		   escalera toca el edificio se puede calcular como altura=3 * seno 85�. Calcule esta altura con una calculadora 
		   y luego escriba un programa en C++ que obtenga y visualice el valor de la altura. 
		  Nota: Los argumentos de todas las funciones trigonom�tricas (seno, coseno, etc) deben estar expresados en radianes. 
		  Por tanto, para obtener el seno, por ejemplo, de un �ngulo expresado en grados, primero deber� convertir el �ngulo 
		  a radianes.
 * @version 1.0
 * @date 06.12.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>
#include <conio.h>
using namespace std;

/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/
#define PI 3.14159265358979

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float globalInputLengthOfTheStaircase = 3.0;
float globalInputStairAngle = 85.0;

float globalInputStairAngleRadians = 0.0;
float globalHeight = 0.0;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
bool CollectData();
bool Calculate();
bool ShowResults();
float CalculateRadians(float angle); //Convierte el angulo en radianes para aplicar la libreria math.h y poder saber el seno del angulo
float CalculateHeight(float angle, float lengthOfTheStaircase); // Ya calcula lo requerido por el usuario

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	if(Run() == false){
		cout<<"Error running the program\r\n"; //Por si el programa ejecuta mal 
	}
	getch();
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

bool Run(){
	while(true){
		if(Calculate() == false){
			cout<<"\r\nError calculating, please try again\r\n";
			continue;
		}
		if(ShowResults() == false){
			cout<<"\r\nError displaying data, try again\r\n";
			continue;
		}
		break;
	}
	
	return true;
}
//=====================================================================================================

bool Calculate(){
	
	globalInputStairAngleRadians = CalculateRadians(globalInputStairAngle);
	globalHeight = CalculateHeight(globalInputStairAngleRadians, globalInputLengthOfTheStaircase);
	
	return true;
}
//=====================================================================================================

bool ShowResults(){
	cout<<"============Show result============\r\n";
	cout<<"\tThe height of the building is "<<globalHeight<<" meters \r\n";
	
	return true;
}
//=====================================================================================================

float CalculateRadians(float angle){
	float radianAngle = 0.0;
	
	radianAngle = PI*(angle)/(180.0);
	
	return radianAngle;
}
//=====================================================================================================

float CalculateHeight(float angle, float lengthOfTheStaircase){
	float height = 0.0;
	
	height = (float)3.0*sin(angle);
	
	return height;
}
