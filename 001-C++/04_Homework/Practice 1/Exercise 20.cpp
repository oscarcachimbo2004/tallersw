/**
 * @file Exercise 20.cpp
 * @author Oscar Lopez (oscar.lopez10@unmsm.edu.pe)
 * @brief Ingresar los lados de un triangulo y el �ngulo que forman, e imprima el  valor del tercer lado, los otros dos �ngulos y el �rea del tri�ngulo.
 * @version 1.0
 * @date 09.12.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>
#include <conio.h>
using namespace std;

/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/
#define PI 3.14159265358979

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float globalInputSide1 = 0.0;
float globalInputSide2 = 0.0;
float globalInputAngle1 = 0.0; 

float globalSide3 = 0.0;
float globalAngle2 = 0.0;
float globalAngle3 = 0.0;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
bool CollectData();
bool Calculate();
bool ShowResults();
float CalculateSide(float side1,float side2,float angle);
float CalculateAngle(float side1,float side2,float side3);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	if(Run() == false){
		cout<<"Error running the program\r\n"; //Por si el programa ejecuta mal 
	}
	
	getch();
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

bool Run(){
	while(true){
		if(CollectData() == false){ //Se presenta el centro de control del programa que verifica si esta bien
			cout<<"\r\nError requesting data, please try again\r\n";
			continue;
		}
		if(Calculate() == false){
			cout<<"\r\nError calculating, please try again\r\n";
			continue;
		}
		if(ShowResults() == false){
			cout<<"\r\nError displaying data, try again\r\n";
			continue;
		}
		break;
	}
	
	return true;
}
//=====================================================================================================

bool CollectData(){
	cout<<"============Insert data============\r\n"; //Solicitamos al usuario lo que nos pide
	cout<<"Calculation in a course\r\n";
	cout<<"\tType the first side of the triangle: ";
	cin>>globalInputSide1;
	//Corroboramos si el numero ingresado,esta en los parametros establecidos
    if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n');
		globalInputSide1 = 0.0;
		cout<<"\r\n\t\tIsn't valid!!!\r\n";
		return false;
	}
	cout<<"\tType the second side of the triangle: ";
	cin>>globalInputSide2;
	if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n');
		globalInputSide2 = 0.0;
		cout<<"\r\n\t\tIsn't valid!!!\r\n";
		return false;
	}
	cout<<"\tEnter the angle formed by these two sides in sexagesimal degrees: ";
	cin>>globalInputAngle1;
	if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n');
		globalInputAngle1 = 0.0;
		cout<<"\r\n\t\tIsn't valid!!!\r\n";
		return false;
	}
	
	if(globalInputSide1 <= 0 || globalInputSide2 <= 0 || globalInputAngle1 <=0){
		
		cout<<"\r\n\t\tIsn't valid!!!\r\n";
		
		return false; 
	}
	
	return true;
}
//=====================================================================================================

bool Calculate(){
	
	globalSide3 = CalculateSide(globalInputSide1,globalInputSide2,globalInputAngle1);
	globalAngle2 = CalculateAngle(globalInputSide1,globalInputSide2,globalSide3);
	globalAngle3 = CalculateAngle(globalInputSide2,globalInputSide1,globalSide3);
	
	return true;
}
//=====================================================================================================

bool ShowResults(){
	cout<<"============Show result============\r\n";
	cout<<"\t\tThe third side of the triangle is "<<globalSide3<<" unites\r\n";
	cout<<"\tThe missing angles of that triangle are....\r\n";
	cout<<"\t\tThe first angle is "<<globalAngle2<<" degrees sexagesimal\r\n";
	cout<<"\t\tThe second angle is "<<globalAngle3<<" degrees sexagesimal\r\n";
	
	return true;
}
//=====================================================================================================

float CalculateSide(float side1,float side2,float angle){
	float radians = 0.0;
	float side = 0.0;
	
	radians = (float)angle*PI/180.0;
	side = (float)sqrt(pow(side1,2)+pow(side2,2)-((2.0)*(side1*side2)*cos(radians)));
	
	return side;
}
//=====================================================================================================

float CalculateAngle(float side1,float side2,float side3){
	float OperationsToFindAngles = 0.0;
	float angle = 0.0;
	
	OperationsToFindAngles = (pow(side1,2) + pow(side3,2) - pow(side2,2))/(2.0*side1*side3); // Previa operacion para hallar el angulo
	angle = (float)(acos(OperationsToFindAngles)); // Con el arco coseno se obtiene el angulo, el procedimiento se deduce de la ley de cosenos
	
	angle = angle * 180.0/PI;
	return angle;
}

