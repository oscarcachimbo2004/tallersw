/**
 * @file Exercise 12.cpp
 * @author Oscar Lopez (oscar.lopez10@unmsm.edu.pe)
 * @brief Escriba un programa para calcular el tiempo transcurrido, en minutos, necesario para hacer un viaje. La ecuaci�n es 
 		  tiempo transcurrido = distancia total/velocidad promedio. Suponga que la distancia est� en kil�metros y la velocidad 
		  en kil�metros/hora. 
 * @version 1.0
 * @date 30.11.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float globalInputDistance = 0.0;
float globalInputVelocity = 0.0;

float globalTimeInHours = 0.0; 
float globalTimeInMinutes = 0.0;
/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
bool CollectData();
bool Calculate();
bool ShowResults();
float CalculateTime(float distance, float velocity);
float CalculateMinutes(float hours);


/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	if(Run() == false){
		cout<<"Error running the program\r\n"; //Por si el programa ejecuta mal 
	}
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

bool Run(){
	while(true){
		if(CollectData() == false){ //Se presenta el centro de control del programa que verifica si esta bien
			cout<<"\r\nError requesting data, please try again\r\n";
			continue;
		}
		if(Calculate() == false){
			cout<<"\r\nError calculating, please try again\r\n";
			continue;
		}
		if(ShowResults() == false){
			cout<<"\r\nError displaying data, try again\r\n";
			continue;
		}
		break;
	}
	
	return true;
}
//=====================================================================================================

bool CollectData(){
	cout<<"============Insert data============\r\n"; //Solicitamos al usuario lo que nos pide
	cout<<"Calculation in a course\r\n";
	cout<<"\tEnter the distance in kilometers: ";
	cin>>globalInputDistance;
	if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n'); //Verificamos el dato
		globalInputDistance = 0.0;
		cout<<"Isn't valid.\r\n";
		return false;
	}
	cout<<"\tDigite la velocidad en km/h: ";
	cin>>globalInputVelocity;
	if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n'); //Verificamos el dato
		globalInputVelocity = 0.0;
		cout<<"Isn't valid.\r\n";
		return false;
	}
	
	if(globalInputDistance <= 0 && globalInputVelocity <= 0){
		
		return false;
	}
	
	return true;
}
//=====================================================================================================

bool Calculate(){
	
	globalTimeInHours = CalculateTime(globalInputDistance, globalInputVelocity);
	globalTimeInMinutes = CalculateMinutes(globalTimeInHours);
	
	return true;
}
//=====================================================================================================

bool ShowResults(){
	cout<<"============Show result============\r\n";
	cout<<"\tThe elapsed time is "<<globalTimeInMinutes<<" min.\r\n";
	
	return true;
}
//=====================================================================================================

float CalculateTime(float distance, float velocity){
	float time = 0.0;
	
	time = (float)(distance)/(velocity);
	
	return time;
}
//=====================================================================================================

float CalculateMinutes(float hours){
	float minutes = 0.0;
	
	minutes = (float)(hours)*60.0;
	
	return minutes;
}
