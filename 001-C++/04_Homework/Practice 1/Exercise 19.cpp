/**
 * @file Exercise 19.cpp
 * @author Oscar Lopez (oscar.lopez10@unmsm.edu.pe)
 * @brief Dados como datos las coordenadas de los tres puntos P1, P2, P3 que corresponden a los v�rtices de un triangulo, 
 		  calcule su per�metro y �rea.
 * @version 1.0
 * @date 08.12.2022
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>
#include <conio.h>
using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float globalInputCoordinateX1 = 0.0;
float globalInputCoordinateY1 = 0.0;
float globalInputCoordinateX2 = 0.0;
float globalInputCoordinateY2 = 0.0;
float globalInputCoordinateX3 = 0.0;
float globalInputCoordinateY3 = 0.0;

float globalTrianglePerimeter = 0.0;
float globalTriangleArea = 0.0;

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
bool Run();
bool CollectData();
bool Calculate();
bool ShowResults();
float CalculatePerimeter(float coordinateX1,float coordinateY1,float coordinateX2,float coordinateY2,float coordinateX3,float coordinateY3);
float CalculateArea(float coordinateX1,float coordinateY1,float coordinateX2,float coordinateY2,float coordinateX3,float coordinateY3);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main(){
	if(Run() == false){
		cout<<"Error running the program\r\n"; //Por si el programa ejecuta mal 
	}
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

bool Run(){
	while(true){
		if(CollectData() == false){ //Se presenta el centro de control del programa que verifica si esta bien
			cout<<"\r\nError requesting data, please try again\r\n";
			continue;
		}
		if(Calculate() == false){
			cout<<"\r\nError calculating, please try again\r\n";
			continue;
		}
		if(ShowResults() == false){
			cout<<"\r\nError displaying data, try again\r\n";
			continue;
		}
		break;
	}
	
	return true;
}
//=====================================================================================================

bool CollectData(){
	cout<<"============Insert data============\r\n"; //Solicitamos al usuario lo que nos pide
	cout<<"Calculation in a course\r\n";
	cout<<"\tCoordinates of P1....\r\n";
	cout<<"\t\tEnter the x1 coordinate: ";
	cin>>globalInputCoordinateX1;
	//Corroboramos si el numero ingresado,esta en los parametros establecidos
    if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n');
		globalInputCoordinateX1 = 0.0;
		cout<<"\r\n\t\tIsn't valid!!!\r\n";
		return false;
	}
	cout<<"\t\tEnter the y1 coordinate: ";
	cin>>globalInputCoordinateY1;
	//Corroboramos si el numero ingresado,esta en los parametros establecidos
    if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n');
		globalInputCoordinateY1 = 0.0;
		cout<<"\r\n\t\tIsn't valid!!!\r\n";
		return false;
	}
	cout<<"\tCoordinates of P2....\r\n";
	cout<<"\t\tEnter the x2 coordinate: ";
	cin>>globalInputCoordinateX2;
	//Corroboramos si el numero ingresado,esta en los parametros establecidos
    if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n');
		globalInputCoordinateX2 = 0.0;
		cout<<"\r\n\t\tIsn't valid!!!\r\n";
		return false;
	}
	cout<<"\t\tEnter the y2 coordinate: ";
	cin>>globalInputCoordinateY2;
	//Corroboramos si el numero ingresado,esta en los parametros establecidos
    if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n');
		globalInputCoordinateY2 = 0.0;
		cout<<"\r\n\t\tIsn't valid!!!\r\n";
		return false;
	}
	cout<<"\tCoordinates of P3....\r\n";
	cout<<"\t\tEnter the x3 coordinate: ";
	cin>>globalInputCoordinateX3;
	//Corroboramos si el numero ingresado,esta en los parametros establecidos
    if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n');
		globalInputCoordinateX3 = 0.0;
		cout<<"\r\n\t\tIsn't valid!!!\r\n";
		return false;
	}
	cout<<"\t\tEnter the y3 coordinate: ";
	cin>>globalInputCoordinateY3;
	//Corroboramos si el numero ingresado,esta en los parametros establecidos
    if(cin.fail() == true){
		cin.clear();
		cin.ignore(1000,'\n');
		globalInputCoordinateY3 = 0.0;
		cout<<"\r\n\t\tIsn't valid!!!\r\n";
		return false;
	}
		
	return true;
}
//=====================================================================================================

bool Calculate(){
	
	globalTrianglePerimeter = CalculatePerimeter(globalInputCoordinateX1,globalInputCoordinateY1,globalInputCoordinateX2,globalInputCoordinateY2,globalInputCoordinateX3,globalInputCoordinateY3);
	globalTriangleArea = CalculateArea(globalInputCoordinateX1,globalInputCoordinateY1,globalInputCoordinateX2,globalInputCoordinateY2,globalInputCoordinateX3,globalInputCoordinateY3);
	
	return true;
}
//=====================================================================================================

bool ShowResults(){
	cout<<"============Show result============\r\n";
	cout<<"The perimeter of the triangle is "<<globalTrianglePerimeter<<" units. \r\n";
	cout<<"The area of the triangle is "<<globalTriangleArea<<" square units. \r\n";	
	
	return true;
}
//=====================================================================================================

float CalculatePerimeter(float coordinateX1,float coordinateY1,float coordinateX2,float coordinateY2,float coordinateX3,float coordinateY3){
	float side1 = 0.0;
	float side2 = 0.0;
	float side3 = 0.0;
	float perimeter = 0.0;
	
	side1 = (float)sqrt(pow(coordinateX1,2)+pow(coordinateY1,2)+pow(coordinateX2,2)+pow(coordinateY2,2));
	side2 = (float)sqrt(pow(coordinateX2,2)+pow(coordinateY2,2)+pow(coordinateX3,2)+pow(coordinateY2,2));
	side3 = (float)sqrt(pow(coordinateX1,2)+pow(coordinateY1,2)+pow(coordinateX3,2)+pow(coordinateY3,2));
	perimeter = (float)side1 + side2 + side3;
	
	return perimeter;
}
//=====================================================================================================

float CalculateArea(float coordinateX1,float coordinateY1,float coordinateX2,float coordinateY2,float coordinateX3,float coordinateY3){
	/*Utilizaremos el metodo de Cramer para saber el area en este triangulo
	
	   I 	X1		Y1     D				Donde :
	X2*Y1 = X2		Y2 = X1*Y2					D = Parte Derecha
	X3*Y2 = X3		Y3 = X2*Y3					I = Parte Izquierdaa
	X1*Y3 = X1		Y1 = X3*Y1
	
				Area = (D-I)/2; (Metodo de Cramer)
	*/
	float rightSide = 0.0;
	float leftSide = 0.0;
	float area = 0.0;
	
	rightSide = (float)(coordinateX1*coordinateY2)+(coordinateX2*coordinateY3)+(coordinateX3*coordinateY1);
	leftSide = (float)(coordinateX2*coordinateY1)+(coordinateX3*coordinateY2)+(coordinateX1*coordinateY3);
	area = (float)(rightSide-leftSide)/2.0;
	
	if(area<0){
		area = (float)(-1.0*area); // Porque el area tiene que ser siempre positivo, y a veces por cramer se pide poner valor absoluto
	}
	
	return area;
}
