/**
 * @file Robot en un arduino.ino
 * @author Oscar Lopez (oscar.lopez10@unmsm.edu.pe)
 * @brief Un robot que es capaz de ser controlado por medio de un dispositivo 
 *        movil donde posteriormente se le aplicara mejoras
 * @version 1.0
 * @date 24.09.2022
 * 
 */
/***************************************************************************************************************
  *                                                INCLUDE
 **************************************************************************************************************/
#include <Arduino.h>

/***************************************************************************************************************
 *  												                  GLOBAL VARIABLES
 **************************************************************************************************************/
char globalReceptionInformation; //Get the information of the bytes in characters

/***************************************************************************************************************
 *  												                  FUNCTION DEFINITION
 **************************************************************************************************************/
void setup(){
  pinMode(13,OUTPUT);   //left motors forward
  pinMode(12,OUTPUT);   //left motors reverse
  pinMode(11,OUTPUT);   //Right motors forward
  pinMode(10,OUTPUT);   //Right motors reverse
  Serial.begin(9600);   //Opens serial port, sets data rate to 9600 bps
  
}
void loop(){
  if(Serial.available()){
  globalReceptionInformation = Serial.read();
  Serial.println(globalReceptionInformation);
  }

  if(globalReceptionInformation == '1'){            //Move forward(all motors rotate in forward direction)
  digitalWrite(13,HIGH);
  digitalWrite(12,LOW);
  digitalWrite(11,HIGH);
  digitalWrite(10,LOW);
}
 
else if(globalReceptionInformation == '2'){      //Move reverse (all motors rotate in reverse direction)
  digitalWrite(13,LOW);
  digitalWrite(12,HIGH);
  digitalWrite(11,LOW);
  digitalWrite(10,HIGH);
}
 
else if(globalReceptionInformation == '3'){ 
  //Turn right (left side motors rotate in forward direction, right  side motors doesn't rotate)
  digitalWrite(13,LOW);
  digitalWrite(12,LOW);
  digitalWrite(11,HIGH);
  digitalWrite(10,LOW);
}
 
else if(globalReceptionInformation == '4'){  
  //Turn left (right side motors rotate in forward direction, left side motors doesn't rotate)
  digitalWrite(13,HIGH);
  digitalWrite(12,LOW);
  digitalWrite(11,LOW);
  digitalWrite(10,LOW);
}
 
else if(globalReceptionInformation == '5'){      //STOP (all motors stop)
  digitalWrite(13,LOW);
  digitalWrite(12,LOW);
  digitalWrite(11,LOW);
  digitalWrite(10,LOW);
}
delay(100);
}